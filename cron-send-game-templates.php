<?php
define("_VALID_PHP", true);
require_once("./init.php");

#region - Start Cron Job
if(isset($_GET)){
	if(isset($_GET["ctkn"]) && isset($_GET["sp"])){
		$cronToken = $_GET["ctkn"];
		$cronShops = $_GET["sp"];
		
		if($cronToken == CRN_TKN){
			#region - Set UTC Time Zone
			date_default_timezone_set('US/Eastern');
			$objCurDateTime = new DateTime();
			#endregion
			
			#region - Get Today's Game Email Templates That Not Send & Time Not Yet Finish
			$gameEmailsTemplates = GameEmail::getGameEmailTempaltesBaseOnDateCondition("sub_date='".$objCurDateTime->format("Y-m-d")."' AND shop='{$cronShops}' AND is_mail_sent = 0");
			#endregion
			
			if(count($gameEmailsTemplates) > 0){
				#region - Loop & Process Through Dates
				foreach($gameEmailsTemplates as $objTemplateInfo){
					#region - Get Game Email Templates Dates
					$masterGameEmailId = $objTemplateInfo->id;
					$subscriptionID = $objTemplateInfo->subscription_id;
					$subscriptionTitle = $objTemplateInfo->title;
					$subscriptionEmailDetails = $objTemplateInfo->email_details;
					$subscriptionDate = $objTemplateInfo->sub_date;
					$templateExpirationTime = $objTemplateInfo->expiration_time;
					$templateMailSendTime = $objTemplateInfo->send_email_time;
					#endregion
					
					#region - Set Game Email Template Dates Object
					$objTmplExprireDateTime = new DateTime($subscriptionDate." ".str_replace(" am", ":00", str_replace(" pm", ":00", $templateExpirationTime)));
					$objTmplSendMailDateTime = new DateTime($subscriptionDate." ".str_replace(" am", ":00", str_replace(" pm", ":00", $templateMailSendTime)));
					#endregion
					
					#region - Get Active Subscriptions & Set Game Mail Templates Expire If Any
					if($objCurDateTime < $objTmplExprireDateTime && $objCurDateTime >= $objTmplSendMailDateTime){
						#region - Fetch Active Subscribers List
						$activeSubscribersInfo = GameEmail::getGameEmailTemplateActiveSubscriber($subscriptionID);
						#endregion
						
						if(count($activeSubscribersInfo) > 0){
							$subscriberEmailsArray = array();
							$subscriptionEndIdsArray = array();
							
							#region - Loop & Get Required Info
							foreach($activeSubscribersInfo as $objSubscrInfo){
								#region - Get & Set Required Variables
								$orderLineItemsMasterId = $objSubscrInfo->orders_line_items_master_id;
								$orderSubscriptionStartDate = $objSubscrInfo->subscription_start_date;
								$orderSubscriptionEndDate = $objSubscrInfo->subscription_end_date;
								$orderSubscriberEmail = $objSubscrInfo->email;
								$ordersubscriberName = $objSubscrInfo->first_name." ".$objSubscrInfo->last_name;
								$gameMailTemplateSendDateTime = $objSubscrInfo->game_mail_template_send_datetime;
								#endregion
								
								#region - Check Subscription End Or Not
								$objSubscriptionStartDate = new DateTime($orderSubscriptionStartDate);
								$objSubscriptionEndDate = new DateTime($orderSubscriptionEndDate);
								$objGameMailSendDateTime = new DateTime($gameMailTemplateSendDateTime);
								
								if($objSubscriptionStartDate <= $objCurDateTime && $objSubscriptionEndDate >= $objCurDateTime){
									
									#region - Temp Mail Send Check Variables
									$objTempCurrentDateTime = new DateTime($objCurDateTime->format("Y-m-d"));
									$objTempMailSendDateTime = new DateTime($objGameMailSendDateTime->format("Y-m-d"));
									#endregion
									
									if($objTempMailSendDateTime < $objTempCurrentDateTime){
										#region - Collect Subscriber Email
										$tempInnerArray = array();
										$tempInnerArray["c_email"] = $orderSubscriberEmail;
										$tempInnerArray["c_name"] = $ordersubscriberName;
										$tempInnerArray["master_id"] = $orderLineItemsMasterId;
										
										$subscriberEmailsArray[] = $tempInnerArray;
										#endregion
									}
								}
								else{
									#region - Collect Subscription End ID
									$subscriptionEndIdsArray[] = $orderLineItemsMasterId;
									#endregion
								}
								#endregion
							}
							#endregion
							
							#region - Send Game Template To Subscriber
							if(count($subscriberEmailsArray) > 0){
								$updateIdsArray = array();
								foreach($subscriberEmailsArray as $objSubscriberInfo){
									GameEmail::sendGameMailTemplate($subscriptionTitle, str_replace("##BREAKLINE##", "<br/>", $subscriptionEmailDetails), $objSubscriberInfo["c_email"], $objSubscriberInfo["c_name"]);
									
									$updateIdsArray[] = $objSubscriberInfo["master_id"];
								}
								
								#region - Set Mail Send DateTime To Order Line Items
								GameEmail::updateMailSendDateTimeForParticularIds(implode(",", $updateIdsArray));
								#endregion
							}
							#endregion
							
							#region - Set Subscription Archive
							if(count($subscriptionEndIdsArray) > 0){
								#region - Set Subscription Archive To Order Line Items
								GameEmail::setSubscriptionArchiveForOrderLineItems(implode(",", $subscriptionEndIdsArray));
								#endregion
							}
							#endregion
						}
					}
					else if($objCurDateTime > $objTmplExprireDateTime){
						#region - Set Current Template Sent
						GameEmail::setGameEmailTemplateSent($masterGameEmailId);
						#endregion
					}
					#endregion
				}
				#endregion
			}
		}
		else{
			print_r("Unauthorized: Access to this resource is denied");
			exit;
		}
	}
	else{
		header("HTTP/1.0 404 Not Found");
		exit;
	}
}
#endregion
?>