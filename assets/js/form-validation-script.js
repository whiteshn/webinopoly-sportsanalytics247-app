var Script = function () {
    $().ready(function () {
        $("#tag-frm").validate({
            rules: {
                desc: {
                    required: true
                },
                tag: {
                    required: true
                },
                default_discount: {
                    required: true,
                    min: 1
                }
            },
            onkeyup: function (element, event) {
                $(event.srcElement).parent(".Polaris-TextField").removeClass('Polaris-TextField--error');
                $(event.srcElement).parents(".polaris-custom-item").find('.Polaris-Labelled__Error').html('');
            },
            errorPlacement: function (error, element) {
                element.parent(".Polaris-TextField").addClass('Polaris-TextField--error');
                error.appendTo(element.parents(".polaris-custom-item").find('.Polaris-Labelled__Error'));
            },
            submitHandler: function (form) {
                form.submit();
//                $("#tag-frm").submit();
            }
        });
        
        $("#product-pricing-frm").validate({
            rules: {
                internal_name: {
                    required: true
                },
                cal_type: {
                    required: true
                },
                customer_tag_id: {
                    required: true,
                },
                price_level: {
                    required: true,
                }
            },
            onkeyup: function (element, event) {
                $(event.srcElement).parent(".Polaris-TextField").removeClass('Polaris-TextField--error');
                $(event.srcElement).parents(".polaris-custom-item").find('.Polaris-Labelled__Error').html('');
            },
            errorPlacement: function (error, element) {
                element.parent(".Polaris-TextField").addClass('Polaris-TextField--error');
                error.appendTo(element.parents(".polaris-custom-item").find('.Polaris-Labelled__Error'));
            },
            submitHandler: function (form) {
                form.submit();
//                $("#product-pricing-frm").submit();
            }
        });
    });
}();