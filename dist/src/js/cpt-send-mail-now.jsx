import React, { Component } from 'react';
import {
Card,
Tabs,
Avatar,
ResourceList,
TextStyle,
List,
Button,
DescriptionList,
EmptyState,
Banner
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';
        
class SendMailNow extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    };  
        
    render() {
        return (
            <Banner
                title="Instant mail send job triggered successfully"
                action={{content: 'Back to Game Emails List', url: 'index.php?do=gamemail'}}
                status="info"
            >
                <p>Make sure you know how these changes affect your store.</p>
            </Banner>
        );
    
    }
}
    
    export default SendMailNow;