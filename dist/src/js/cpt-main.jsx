import React, { Component } from 'react';
import {
Link,
Card,
Tabs,
ResourceList,
TextStyle,
Avatar
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';
        
class Main extends Component {
  
        constructor(props) {
            super(props);

            this.state = {
                selected: 0,
            }
        };

        handleTabChange(selectedTabIndex){
         this.setState({selected: selectedTabIndex});
        };
        
         renderItem(item){  
            const {id, url, name, email,ViewSubscriptionUrl} = item;
            const media = <Avatar customer size="medium" name={name} />;
            const shortcutActions = ViewSubscriptionUrl
            ? [{content: 'View subscriptions', url: ViewSubscriptionUrl}]
            : null;

            return (
              <ResourceList.Item
                id={id}
                url={"#"}
                media={media}
                accessibilityLabel={`View details for ${name}`}
                shortcutActions={shortcutActions}
                persistActions
              >
               <Link url={url} external={true}>
                <h3>
                  <TextStyle variation="strong">{name}</TextStyle>
                </h3>
                <div>{email}</div>
               </Link>
              </ResourceList.Item>
            );
          };
      
        
        bindCustomerData(){
            let dataRows = [], globObj = this, resourceList = [];
            if(customersData.length > 0){
               customersData.forEach(function(drow) {
                  if(globObj.state.selected == 0){
                       if(drow.is_subscription_active == 1){
                           dataRows.push(drow);
                       }
                   }else if(globObj.state.selected == 1){
                      if(drow.is_subscription_active == 0){
                           dataRows.push(drow);
                       } 
                   } 
               });
               
               if(dataRows.length > 0){
                    dataRows.forEach(function(drow){
                        let tempObj = {};
                        tempObj.id = drow.id;
                        tempObj.url = 'https://'+drow.shop+'/admin/customers/'+drow.shopify_customer_id;
                        tempObj.name = drow.first_name+' '+drow.last_name;
                        tempObj.email = drow.email;
                        tempObj.ViewSubscriptionUrl = SITEURL + '/index.php?do=subscription&cust_id='+drow.shopify_customer_id+"&sub_type="+drow.is_subscription_active;
                        resourceList.push(tempObj);
                    });
               }
               
               return (
                    <ResourceList
                        resourceName={{singular: 'customer', plural: 'customers'}}
                        items={resourceList}
                        renderItem={this.renderItem}            
                    />    
               );
            }
        };
        
       render() {
            const {selected} = this.state;
            const tabs = [
              {
                id: 'active-customers',
                content: 'Active customers',
                accessibilityLabel: 'Active customers',
                panelID: 'active-customers-content',
              },
              {
                id: 'archive-customers',
                content: 'Archive customers',
                accessibilityLabel: 'Archive customers',
                panelID: 'archive-customers-content',
              }
            ];
        if(customersData.length > 0){
            return (
              <Card>
                <Tabs
                  tabs={tabs}
                  selected={selected}
                  onSelect={this.handleTabChange.bind(this)}
                  fitted
                />
                <Card.Section title={tabs[selected].content}>
                  {this.bindCustomerData()}
                </Card.Section>
              </Card>
            );
        }else{
            return (
                <Card title="Subscriptions" sectioned>
                    <p>View a summary of your customers.</p>
                </Card>
            );
        }
     }
    }
    
    export default Main;