import React, { Component } from 'react';
import {
        Layout,
        Page
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';

import Main from './cpt-main.jsx';
import Footer from './cpt-footer.jsx';

class Index extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        return (
                <Page fullWidth={true}>
                    <Layout>
                        <Layout.Section>
                            <Main />
                        </Layout.Section>
                    </Layout>
                </Page>
                                );
                    }
                }

        export default Index;