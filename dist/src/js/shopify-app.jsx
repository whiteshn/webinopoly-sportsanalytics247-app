/*-----Import GLOBAL START------*/
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import * as GLOB_CONST from './global-constants.jsx';
import {
AppProvider,
        Layout,
        Page
} from '@shopify/polaris';
/*-----Import GLOBAL END------*/

/*-----Import Sub Componenets START------*/
import Main from './cpt-main.jsx';
import Footer from './cpt-footer.jsx';
import Subscription from './cpt-subscription.jsx';
import GameEmailAdd from './cpt-gamemailadd.jsx';
import GameEmail from './cpt-game_email.jsx';
import SendMailNow from './cpt-send-mail-now.jsx';

/*-----Import Sub Componenets END------*/



class ShopifyApp extends Component {

    constructor(props) {
        super(props);

        var pageName = window.location.href.split("/")[window.location.href.split("/").length - 1];
        if (pageName.indexOf("?") > -1) {
            var name = ('do').replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
            var results = regex.exec(location.search);
            pageName = results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        }
        if (pageName.indexOf("#") > -1)
            pageName = pageName.split("#")[0];
        if (pageName.indexOf(".") > -1)
            pageName = pageName.split(".")[0];

        this.state = {
            route: pageName
        };
    }
    
    render() {
        
        var Child;
        var breadcrumbs = [];
        switch (this.state.route.toLowerCase()) {
            case 'index':
                Child = Main; // render index page
                break;
            case 'subscription':
                Child = Subscription; // render subscription page
                break;
            case 'gameemail-add':
                Child = GameEmailAdd; // render game mail add page
                break;
            case 'gamemail':
                Child = GameEmail; // render game mail list page
                break;
            case 'send-mail-now':
                Child = SendMailNow; // render send mail now page
                break;
            default:
                Child = Main; // render index page
                break;
        }
        return (
                <Page fullWidth={true}>
                    <Layout>
                        <Layout.Section>
                            <Child />
                        </Layout.Section>
                    </Layout>
                    <Footer />
                </Page>

                );
        //        return (<Child />);
    }
}



if (document.getElementById('root') !== null)
{
    ReactDOM.render(
            <AppProvider apiKey={SHOPIFY_APP_API_KEY} shopOrigin={SHOPIFY_APP_STORE}>
                <ShopifyApp />
            </AppProvider>
            , document.getElementById('root')
            );
}