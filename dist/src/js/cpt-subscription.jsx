import React, { Component } from 'react';
import {
Card,
DataTable,
Badge
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';
        
class Subscription extends Component {
  
        constructor(props) {
            super(props);

            this.state = {
                sortedRows:null
            }
        };

        
        bindSubscriptionData(){
            let dataRows = [], globObj = this, status = ['Expired', 'Active'];
            if(subscriptionData.length > 0){
               subscriptionData.forEach(function(drow) {
                    let badgeStatus = (<Badge>{status[drow.is_subscription_archive]}</Badge>);
                   if(drow.is_subscription_archive){
                    badgeStatus = (<Badge status="success">{status[drow.is_subscription_archive]}</Badge>);
                   }
                    dataRows.push([drow.product_title,drow.subscription_start_date,drow.subscription_end_date, badgeStatus]);
               });
               return dataRows;
            }
        };
          
       render() {
            const rows = this.bindSubscriptionData();
            if(subscriptionData.length > 0){
                return (
                    <div id="subscription">
                      <Card>
                        <DataTable
                          columnContentTypes={[
                            'text',
                            'text',
                            'text',
                            'text'
                          ]}
                          headings={[
                            'Subscription name',
                            'Subscription start date',
                            'Subscription end date',
                            'Status'
                          ]}
                           rows={rows}
                           defaultSortDirection="descending"
                           initialSortColumnIndex={1}
                           footerContent={`Showing ${rows.length} of ${rows.length} results`}
                        />
                      </Card>
                    </div>      
                );
            }else{
                return (
                  <Card title="Subscriptions" sectioned>
                    <p>View a summary of your subscriptions.</p>
                  </Card>
                );
            }
        }
    }

    export default Subscription;