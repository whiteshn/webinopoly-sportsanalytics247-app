import React, { Component } from 'react';
import {
Card,
Tabs,
Avatar,
ResourceList,
TextStyle,
List,
Button,
DescriptionList,
EmptyState
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';
        
class GameEmail extends Component {
  
        constructor(props) {
            super(props);

            this.state = {
                selected: 0,
            }
        };

        handleTabChange(selectedTabIndex){
         this.setState({selected: selectedTabIndex});
        };

        sendMailNow(){
            console.log("event fired");
        }

        addMailSendButton(){
            return (
                <Button primary onClick={this.sendMailNow.bind(this)}>Send Mail</Button>
            );
        }
        
         renderItem(item){
            let objThis = this;
            const {id, title, sub_title, date, expire_time, send_mail_time,sub_img} = item;
            const media = <Avatar name={sub_title} source={sub_img} size="large" customer={false}/>;
            const desclist = [];
            desclist.push({term:"Subscription",description:sub_title},{term:"Date",description:date},{term:"Expiration time",description:expire_time},{term:"Send email time",description:send_mail_time});
            const sendMailBTN = <Button primary>Send Mail</Button>;
            
            return (
              <ResourceList.Item
                id={id}
                url={"#"}
                media={media}
                persistActions={true}
                shortcutActions={[{
                    content: "Send Mail Now",
                    primary: true,
                    url: "index.php?do=send-mail-now&sid=" + id
                }]}
              >
                <div className="manage-child-elements">
                    <h3>
                        <TextStyle variation="strong">{title}</TextStyle>
                    </h3>
                </div>
                <DescriptionList
                    items={desclist}
                 />
                 
              </ResourceList.Item>
            );
          };
      
        
        bindGameMailData(){
            let dataRows = [], globObj = this, resourceList = [];
            if(gameMails.length > 0){
               gameMails.forEach(function(drow) {
                  if(globObj.state.selected == 0){
                       if(drow.is_mail_sent == 0){
                           dataRows.push(drow);
                       }
                   }else if(globObj.state.selected == 1){
                      if(drow.is_mail_sent == 1){
                           dataRows.push(drow);
                       } 
                   } 
               });
               
               if(dataRows.length > 0){
                    dataRows.forEach(function(drow){
                        let tempObj = {};
                        tempObj.id = drow.id;
                        tempObj.title = drow.title;
                        tempObj.sub_title = drow.subscription_title;
                        tempObj.date = drow.sub_date;
                        tempObj.expire_time = drow.expiration_time;
                        tempObj.send_mail_time = drow.send_email_time;
                        tempObj.sub_img= drow.store_product_feature_img;
                        resourceList.push(tempObj);
                    });
               }
               
               return (
                    <ResourceList
                        resourceName={{singular: 'customer', plural: 'customers'}}
                        items={resourceList}
                        renderItem={this.renderItem}            
                    />    
               );
            }
        };
        addGameEmail(){
            alert('hi');
            location.href = '/index.php?do=gameemail-add';
        };
        
       render() {
            const {selected} = this.state;
            const tabs = [
              {
                id: 'pending-gamemail',
                content: 'Pending game emails',
                accessibilityLabel: 'Pending game emails',
                panelID: 'pending-gamemail-content',
              },
              {
                id: 'sent-gamemail',
                content: 'Sent game emails',
                accessibilityLabel: 'Sent game emails',
                panelID: 'sent-gamemail-content',
              }
            ];
        if(gameMails.length > 0){
            return (
              <Card sectioned title="&nbsp;" actions={[{content:<Button primary icon="add">Add game email</Button>,url: SITEURL+"/index.php?do=gameemail-add"}]}>
                <Tabs
                  tabs={tabs}
                  selected={selected}
                  onSelect={this.handleTabChange.bind(this)}
                  fitted
                />
                <Card.Section title={tabs[selected].content}>
                  {this.bindGameMailData()}
                </Card.Section>
              </Card>
            );
        }else{
            return (
                <EmptyState
                    heading="Manage your game emails"
                    action={{content: 'Add game email',url: "index.php?do=gameemail-add"}}
                    image="https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg"
                  >
                    <p>You will see all game mails here.</p>
                </EmptyState>
            );
        }
     }
    }
    
    export default GameEmail;