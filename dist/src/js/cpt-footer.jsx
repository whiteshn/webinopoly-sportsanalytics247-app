import React, { Component } from 'react';
import {
        Link,
        FooterHelp
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';
class Footer extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    render() {
        return (
                <FooterHelp>
                    © 2018 Made with ♡ by{' '}
                    <Link url="#">
                    Webinopoly Inc.
                    </Link>
                    - The Shopify Experts
                </FooterHelp>
                );
    }
}

export default Footer;