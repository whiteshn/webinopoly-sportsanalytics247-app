import 'rc-time-picker/assets/index.css';
import React, { Component } from 'react';
import moment from 'moment';
import TimePicker from 'rc-time-picker';
import {
Card,
EmptyState,
Form,
Select,
TextField,
Button,
FormLayout,
DatePicker,
TextStyle,
PageActions,
Banner,
ExceptionList,
Toast,
Frame
} from '@shopify/polaris';
import '@shopify/polaris/styles.css';
        
class GameEmailAdd extends Component {
        constructor(props) {
            super(props);
            let todayDate = new Date();

            /* Set & Get EST DateTime Start */
            var estOffset = -5.0

            var systemDateTime = new Date();
            var currentUTC = systemDateTime.getTime() + (systemDateTime.getTimezoneOffset() * 60000);

            var estDateTime = new Date(currentUTC + (3600000 * estOffset));
            /* Set & Get EST DateTime End */

            this.state = {
                display_est_dt: estDateTime.toLocaleString(),
                month: todayDate.getMonth(),
                year: todayDate.getFullYear(),
                isloading:false,
                isShowError:false,
                isSaveSuccess:false,
                isSaveError:false,
                msgSaveSuccess:"Game email added",
                msgSaveError:"We were unable to save the game email",
                showSecond:false,
                selected: {
                    start: new Date(),
                    end: new Date(),
                },
                shop:SHOPIFY_STORE,
                subscription_title:'',
                subscription_id: '',       
                date: "",
                expiration_time: moment(),
                comments: '',
                title: '',
                email_details: '',
                send_email_time: moment(),
                isValidate:{
                    is_subscription_id:true,
                    is_date:true,
                    is_comments:true,
                    is_title:true,
                    is_email_details:true,
                    is_expiration_time:true,
                    is_send_email_time:true
                },
                msgValidate:[]
                
                
            }       
        };
        d2(n){
            if(n<9) return "0"+n;
            return n;
	};
        getSelectOptionText(sel) {
            return sel.options[sel.selectedIndex].text;
        };
        componentDidMount(){
            let today = new Date(),
                dformat = today.getFullYear() + "-" + this.d2(parseInt(today.getMonth()+1)) + "-" + this.d2(today.getDate()) + " " + this.d2(today.getHours()) + ":" + this.d2(today.getMinutes()) + ":" + this.d2(today.getSeconds()),
                format = this.state.showSecond ? 'h:mm:ss a' : 'H:mm a',
                expiration_time_val = this.state.expiration_time.format(format),
                send_email_time_val = this.state.send_email_time.format(format),
                subscription_id = document.getElementById('subscription_id').value,
                subscription_title = this.getSelectOptionText(document.getElementById('subscription_id'));
        
           this.setState({date:dformat,send_email_time:send_email_time_val,expiration_time:expiration_time_val,subscription_id:subscription_id,subscription_title:subscription_title});
        }
        
        
         bindpkgOptions(){
            let dataRows = [], globObj = this;
            if(pkgOptions.length > 0){
               pkgOptions.forEach(function(drow) {
                    dataRows.push({label: drow.store_product_title, value: drow.id});
               });
               return dataRows;
            }
        };

          
       render() {
            const {subscription_id, subscription_title, date, expiration_time, comments, title, email_details, send_email_time,month, year, selected} = this.state;
            const options = this.bindpkgOptions();
            
            
        return <div id="game-email-form">
                {this.state.isShowError ? 
                <div id="error-banner">
                  <Banner status="critical" icon="alert" title="Please enter below required details">
                    <ExceptionList
                        items={this.state.msgValidate}
                    />
                  </Banner>
                  <br/>
                </div>  
                :null
                }
                
                <Form onSubmit={this.handleSubmit} implicitSubmit={false} name="game_email_frm">
                    <FormLayout>
                       <Select
                            id="subscription_id"
                            label="Subscription"
                            labelInline
                            options={options}
                            onChange={this.handleChange('subscription_id')}
                            value={this.state.subscription_id}
                        /> 
                        <TextStyle variation="strong">Date</TextStyle>
                         <DatePicker
                            month={month}
                            year={year}
                            onChange={this.onDateChange.bind(this,'date')}
                            onMonthChange={this.handleMonthChange.bind(this)}
                            selected={selected}
                            disableDatesBefore={this.handleDisableDate()}
                         />
                        <TextStyle variation="strong">Expiration time</TextStyle>
                        <TimePicker
                            style={{ width: 100 }}
                            showSecond={this.state.showSecond}
                            defaultValue={this.state.expiration_time}
                            onChange={this.onTimeChange.bind(this,'expiration_time')}
                            inputReadOnly
                         />
                        <TextStyle variation="strong">Following EST Timezone(Current: {this.state.display_est_dt})</TextStyle>
                        <TextStyle variation="strong">Comments</TextStyle>
                        <TextField label="Comments" value={this.state.comments} onChange={this.handleChange('comments')} multiline={5} labelHidden={true}/>
                        <TextStyle variation="strong">Title / Subject</TextStyle>
                        <TextField label="Title / Subject" value={this.state.title} onChange={this.handleChange('title')} labelHidden={true} />
                        <TextStyle variation="strong">Email details</TextStyle>
                        <TextField label="Email details" value={this.state.email_details} onChange={this.handleChange('email_details')} multiline={10} labelHidden={true}/>
                        <TextStyle variation="strong">Send mail time</TextStyle>
                        <TimePicker
                            style={{ width: 100 }}
                            showSecond={this.state.showSecond}
                            defaultValue={this.state.send_email_time}
                            onChange={this.onTimeChange.bind(this,'send_email_time')}
                            inputReadOnly
                         />
                         <TextStyle variation="strong">Following EST Timezone(Current: {this.state.display_est_dt})</TextStyle>
                         <PageActions
                            primaryAction={{
                              content: 'Save',
                              loading:this.state.isloading,
                              onClick:this.onSaveClick.bind(this)
                            }}
                            secondaryActions={[
                              {
                                content: 'Cancel'
                              },
                            ]}
                          />
                    </FormLayout>
              </Form>
            </div>;
        }
        toggleToast() {
            this.setState({showToast: !showToast});
        };
        handleSubmit(event){
            this.setState({ subscription_id:'', date:'', expiration_time:'', comments:'', title:'', email_details:'', send_email_time:'' });
        };

        handleChange(field){
          let obj = this;  
          return (value) => this.setState({[field]:value},function(){
            let subscription_title = this.getSelectOptionText(document.getElementById('subscription_id'));
            obj.setState({subscription_title:subscription_title});  
          });
        };
        handleMonthChange(month, year){
            this.setState({
              month,
              year
            });
        };
        handleDisableDate(){
            var d = new Date();
            d.setDate(d.getDate()-1);
            return d;
        }
        onTimeChange(field,value) {
           let format = this.state.showSecond ? 'h:mm:ss a' : 'H:mm a';
           value = value.format(format);
           this.setState({[field]:value});
        }
        onDateChange(field,value) {
           let format = 'H:mm a';
           let date = value.start.getFullYear() + "-" + this.d2(parseInt(value.start.getMonth()+1)) + "-" + this.d2(value.start.getDate()) + " " + this.d2(value.start.getHours()) + ":" + this.d2(value.start.getMinutes()) + ":" + this.d2(value.start.getSeconds());
           this.setState({[field]:date});
        }
        onSubmitValidate(){
            let validateArr = {},msgValidate = [];
            if(this.state.subscription_id == ''){
                validateArr.subscription_id = false;
                msgValidate.push({icon: 'alert',description: 'Subscription is required'});
            }
            if(this.state.date == ''){
                validateArr.is_date = false;
                msgValidate.push({icon: 'alert',description: 'Date is required'});
            }
            if(this.state.expiration_time == ''){
                validateArr.is_expiration_time = false;
                msgValidate.push({icon: 'alert',description: 'Expiration time is required'});
            }
            if(this.state.comments == ''){
                validateArr.is_comments = false;
                msgValidate.push({icon: 'alert',description: 'Comments is required'});
            }
            if(this.state.title == ''){
                validateArr.is_title = false;
                msgValidate.push({icon: 'alert',description: 'Title / Subject is required'});
            }
            if(this.state.email_details == ''){
                validateArr.is_email_details = false;
                msgValidate.push({icon: 'alert',description: 'Email details is required'});
            }
            if(this.state.send_email_time == ''){
                validateArr.is_send_email_time = false;
                msgValidate.push({icon: 'alert',description: 'Send email time is required'});
            }
            if(msgValidate.length > 0){
                this.setState({msgValidate:msgValidate});    
            }
            return validateArr;
        };
        onSaveClick(){
            let frmData = {},obj=this;
            this.setState({isloading:true,isShowError:false,msgValidate:[]});
            let isValidate = this.onSubmitValidate(this);
            if(Object.getOwnPropertyNames(isValidate).length == 0){
                frmData.subscription_id = this.state.subscription_id;
                frmData.subscription_title = this.state.subscription_title;
                frmData.date = this.state.date;
                frmData.expiration_time = this.state.expiration_time;
                frmData.comments = this.state.comments;
                frmData.title = this.state.title;
                frmData.email_details = this.state.email_details;
                frmData.send_email_time = this.state.send_email_time;
                frmData.shop = this.state.shop;
                frmData.processGameEmail = 1;
                $.post("controller.php", frmData, function(resultData){
                    var resultObj = JSON.parse(resultData);
                    if(resultObj.type == 'success'){
                        obj.setState({isloading:false,isShowError:false,msgValidate:[],isSaveSuccess:true,msgSaveSuccess:resultObj.message},function(){
                            ShopifyApp.flashNotice(this.state.msgSaveSuccess);    
                            obj.setState({subscription_id:'',subscription_title:'',date:'',expiration_time:moment(),comments:'',title:'',email_details:'',send_email_time:moment()});
                        });
                        
                        location.href = 'index.php?do=gamemail';
                        
                    }else{
                        obj.setState({isloading:false,isShowError:false,msgValidate:[],isSaveError:true,msgSaveError:resultObj.message},function(){
                            ShopifyApp.flashError(this.state.msgSaveError);    
                        });   
                        location.href = 'index.php?do=gamemail';
                    }
                });
            }else{                
             this.setState({isShowError:true,isloading:false,isValidate:isValidate});
             setScrollTop();
            }
        }
        
    }

    export default GameEmailAdd;