<?php

/**
 * Controller
 *
 * @package Membership Manager Pro
 * @author wojoscripts.com
 * @copyright 2015
 * @version $Id: controller.php, v3.00 2015-02-10 10:12:05 gewa Exp $
 */
define("_VALID_PHP", true);

require_once("init.php");

$delete = (isset($_POST['delete'])) ? $_POST['delete'] : null;
?>
<?php

switch ($delete):
    /* == Delete Media == */
    case "deleteTag":
        $res = $db->delete(CustomerTags::ctTable, "id=" . Filter::$id);
        if ($res) :
            $json['type'] = 'success';
            $json['title'] = Core::$word->SUCCESS;
            $json['message'] = 'Tag deleted successfully';
        else :
            $json['type'] = 'warning';
            $json['title'] = Core::$word->ALERT;
            $json['message'] = Core::$word->SYSTEM_PROCCESS;
        endif;
        print json_encode($json);
        break;
    case "deletePricingGroup":
        $res = $db->delete(WholesalePricing::wpTable, "id=" . Filter::$id);
        if ($res) {
            @$db->delete(BatchAction::baTable, "group_id=" . Filter::$id);
        }
        if ($res) :
            $json['type'] = 'success';
            $json['title'] = Core::$word->SUCCESS;
            $json['message'] = 'Pricing group deleted successfully';
        else :
            $json['type'] = 'warning';
            $json['title'] = Core::$word->ALERT;
            $json['message'] = Core::$word->SYSTEM_PROCCESS;
        endif;
        print json_encode($json);
        break;
endswitch;

if (isset($_POST['processGameEmail'])):
    $msg = $GameEmail->processGameEmail();
    print $msg;
endif;

?>
