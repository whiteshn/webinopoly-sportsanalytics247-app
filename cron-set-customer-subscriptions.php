<?php
define("_VALID_PHP", true);
require_once("./init.php");

#region - Start Cron Job
if(isset($_GET)){
	if(isset($_GET["ctkn"]) && isset($_GET["sp"])){
		$cronToken = $_GET["ctkn"];
		$cronShops = $_GET["sp"];
		
		if($cronToken == CRN_TKN){
			#region - Get Active Subscription Customers
			$activeSubscriptionsCustomers = GameEmail::getActiveSubscriptionCustomers();
			#endregion
			
			if(count($activeSubscriptionsCustomers) > 0){
				$activeCustomersShopifyIDSArray = array();
				
				#region - Loop & Collect All Active Customers Shopify Customer ID
				foreach($activeSubscriptionsCustomers as $objCustomerInfo){
					$activeCustomersShopifyIDSArray[] = $objCustomerInfo->shopify_customer_id;
				}
				#endregion
				
				if(count($activeCustomersShopifyIDSArray) > 0){
					$activeSubscrCustIDSArray = array();
					#region - Get All Active Subscriptons Order Line Items
					$activeSubscriptionsInfo = GameEmail::getActiveSubscriptionsByCustIDS(implode(",", $activeCustomersShopifyIDSArray));
					#endregion
					
					$customerIdsForArchiveArray = array();
					foreach($activeSubscriptionsCustomers as $objCustomerInfo){
						$tempMasterId = $objCustomerInfo->master_customer_id;
						$tempShopifyCustomerId = $objCustomerInfo->shopify_customer_id;
						
						#region - Check Customer Exist In Active List
						$isExistInActive = false;
						if(count($activeSubscriptionsInfo) > 0){
							foreach($activeSubscriptionsInfo as $objSubscrInfo){
								if($tempShopifyCustomerId == $objSubscrInfo->shopify_customer_id && !$isExistInActive){
									$isExistInActive = true;
								}
							}
						}
						
						if(!$isExistInActive){
							$customerIdsForArchiveArray[] = $tempMasterId;
						}
						#endregion
					}
					
					if(count($customerIdsForArchiveArray) > 0){
						#region - Set Customers Inactive
						GameEmail::setCustomersSubscriptionsArchive(implode(",", $customerIdsForArchiveArray));
						#endregion
					}
				}
			}
		}
		else{
			print_r("Unauthorized: Access to this resource is denied");
			exit;
		}
	}
	else{
		header("HTTP/1.0 404 Not Found");
		exit;
	}
}
#endregion
?>