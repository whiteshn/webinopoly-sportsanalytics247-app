<?php

/**
 * wrapper class of Graphiql for shopify
 * 
 *
 * @package Garaphql
 * @author Nik Suthar <nik@webinopoly.com>
 * */
use PHPShopify\CurlRequest;

class Graphql {

    static $headers = array();
    static $domain = '';
    static $url = '';
    static $_curl;

    const regex = <<<'END'
/
  (
    (?: [\x00-\x7F]               # single-byte sequences   0xxxxxxx
    |   [\xC0-\xDF][\x80-\xBF]    # double-byte sequences   110xxxxx 10xxxxxx
    |   [\xE0-\xEF][\x80-\xBF]{2} # triple-byte sequences   1110xxxx 10xxxxxx * 2
    |   [\xF0-\xF7][\x80-\xBF]{3} # quadruple-byte sequence 11110xxx 10xxxxxx * 3 
    ){1,100}                      # ...one or more times
  )
| ( [\x80-\xBF] )                 # invalid byte in range 10000000 - 10111111
| ( [\xC0-\xFF] )                 # invalid byte in range 11000000 - 11111111
/x
END;

    /**
     * @var float microtime of last api call
     */
    public static $microtimeOfLastApiCall;

    /**
     * @var float Minimum gap in seconds to maintain between 2 api calls
     */
    public static $timeAllowedForEachApiCall = .5;

    function __construct($domain, $headers, $search = false) {
        if ($headers == '') {
            return false;
        }

        self::$domain = $domain;
        if (self::$domain != '' && !empty($headers) && is_array($headers)) {
            self::$url = 'https://' . self::$domain . '/admin/api/graphql.json';
            self::$headers = $headers;
        }
        self::$_curl = new CurlRequest();
    }

    public static function checkApiCallLimit($firstCallWait = false) {
        $timeToWait = 0;
        if (static::$microtimeOfLastApiCall == null) {
            if ($firstCallWait) {
                $timeToWait = static::$timeAllowedForEachApiCall;
            }
        } else {
            $now = microtime(true);
            $timeSinceLastCall = $now - static::$microtimeOfLastApiCall;
            //Ensure 2 API calls per second
            if ($timeSinceLastCall < static::$timeAllowedForEachApiCall) {
                $timeToWait = static::$timeAllowedForEachApiCall - $timeSinceLastCall;
            }
        }

        if ($timeToWait) {
            //convert time to microseconds
            $microSecondsToWait = $timeToWait * 1000000;
            //Wait to maintain the API call difference of .5 seconds
            usleep($microSecondsToWait);
        }

        static::$microtimeOfLastApiCall = microtime(true);
    }

    private function graphQlPost($query, $search = false) {
        self::checkApiCallLimit();
        if ($search == true) {
            self::$headers['Content-type'] = 'application/graphql';
        } else {
            self::$headers['Content-type'] = 'application/json;charset=utf-8';
        }
        return self::$_curl->post(self::$url, $query, self::$headers);
    }

    public function createDraftOrderOnStore($input) {
        $query = <<<'JSON'
mutation($input:DraftOrderInput!){
  draftOrderCreate(input:$input){
    draftOrder{
      id
      invoiceUrl
    }
    userErrors{
      field
      message
    }
  }
}
JSON;
        $variables = array('input' => $input);
        $json = json_encode(['query' => $query, 'variables' => $variables]);
        $qlres = $this->graphQlPost($json);
        try {
            if ($qlres != '') {
                $storeRes = json_decode($qlres, true);
                return (empty(@$storeRes['data']['draftOrderCreate']['userErrors']) ? array('status' => 1, 'order' => @$storeRes['data']['draftOrderCreate']['draftOrder']) : array('status' => 0));
            }
        } catch (Exception $ex) {
            return array('status' => 0);
        }
    }

    public function getOrder($orderid) {
        $qlQuery = <<<'JSON'
query{
  order(id:"gid://shopify/Order/$orderid"){
    id
    name
    note
    subtotalPrice
    subtotalLineItemsQuantity
    totalPrice
    totalTax
    shippingAddress{
      name
      address1
      address2
      city
      province
      zip
      phone
    }
    customer{
      firstName
      lastName
      displayName
      email
      phone
    }
  }
}
JSON;
        $qlQuery = str_replace(array('$orderid'), array($orderid), $qlQuery);
        $qlres = $this->graphQlPost($qlQuery, true);
        try {
            if ($qlres != '') {
                $metaFields = json_decode($qlres, true);
                return (!empty(@$metaFields['data']['order']) ? @$metaFields['data']['order'] : 0);
            }
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getLineItemNode($lineitemid) {
        $qlQuery = <<<'JSON'
query{
  node(id:"$lineitemid"){
    ... on LineItem{
      originalTotal
      originalUnitPrice
      discountedTotal
      discountedUnitPrice
      product{
        description
        productType
        metafields(first:20){
       	edges{
            node{
              description
              key
              value
            }
          }   
        }
      }
    }
  }
}
JSON;
        $qlQuery = str_replace(array('$lineitemid'), array($lineitemid), $qlQuery);
        $qlres = $this->graphQlPost($qlQuery, true);
        try {
            if ($qlres != '') {
                $metaFields = json_decode($qlres, true);
                return (!empty(@$metaFields['data']['node']) ? @$metaFields['data']['node'] : 0);
            }
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getProductVariantMeta($variantid) {
        $qlQuery = <<<'JSON'
query{
  productVariant(id:"gid://shopify/ProductVariant/$variantid"){
    metafields(first:20){
      edges{
        node{
          id
          key
          namespace
          value
          valueType
        }
      }
    }
  }
}
JSON;
        $qlQuery = str_replace(array('$variantid'), array($variantid), $qlQuery);
        $qlres = $this->graphQlPost($qlQuery, true);
        try {
            if ($qlres != '') {
                $metaFields = json_decode($qlres, true);
                return (!empty(@$metaFields['data']['productVariant']['metafields']) ? @$metaFields['data']['productVariant']['metafields'] : 0);
            }
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function getProductVendors() {
        $qlQuery = <<<'JSON'
query{
  shop{
    productVendors(first:250){
      edges{
        node
      }
    }
  }
}
JSON;
        $qlres = $this->graphQlPost($qlQuery, true);
        try {
            if ($qlres != '') {
                $vendors = json_decode($qlres, true);
                return (!empty(@$vendors['data']['shop']['productVendors']['edges']) ? @$vendors['data']['shop']['productVendors']['edges'] : 0);
            }
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function deleteSingleMetafield($metaGid) {
        $qlQuery = '
               mutation{
                metafieldDelete(input:{id:"' . $metaGid . '"}){
                  userErrors{
                    field
                    message
                  }
                  deletedId
                }
              }';

        $qlres = $this->graphQlPost($qlQuery, true);

        try {
            if ($qlres != '') {
                $metafieldDelete = json_decode($qlres, true);
                return (@$metafieldDelete['data']['metafieldDelete']['deletedId'] != '' ? 1 : 0);
            }
        } catch (Exception $ex) {
            return 0;
        }
    }

}
