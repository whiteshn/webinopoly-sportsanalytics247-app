<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class Customer {

    const custMasterTbl = "store_customer_master";

    private static $db;

    /**
     * Content::__construct()
     * 
     * @return
     */
    function __construct() {
        self::$db = Registry::get("Database");
    }

    public static function getCusotmers($shop) {
        $sql = "select * from " . self::custMasterTbl;
        ;
        $Customers = self::$db->fetch_all($sql, true);
        return $Customers;
    }

    public static function getArchiveCustomers($shop) {
        $sql = "select * from " . self::custMasterTbl . " where is_subscription_active=0";
        $archiveCustomers = self::$db->fetch_all($sql, true);
        return $archiveCustomers;
    }

    public static function getActiveCustomers($shop) {
        $sql = "select * from " . self::custMasterTbl . " where is_subscription_active=1";
        $activeCustomers = self::$db->fetch_all($sql, true);
        return $activeCustomers;
    }

    protected static function addMasterCustomer($insertdata) {
        $insertedId = self::$db->insert(self::custMasterTbl, $insertdata);
        return $insertedId;
    }

    protected static function updateCustomerMaster($updatedata, $shopifyCustomerId, $shop) {
        $updatedId = self::$db->update(self::custMasterTbl, $updatedata, "shopify_customer_id='$shopifyCustomerId' and shop='$shop'");
        return $updatedId;
    }

    protected static function deleteMasterCustomer($shopifyCustomerId, $shop) {
        $retunrMasterCustomerId = self::$db->delete(self::custMasterTbl, "shopify_customer_id=$shopifyCustomerId and shop='$shop'");
        return $retunrMasterCustomerId;
    }

    public static function shopifyCustomerCreateWebhook() {
        if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
            $data = file_get_contents('php://input');
            $verified = verify_webhook($data, $hmac_header);
            if ($verified) {
                extract($_GET);
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];
                $decodeJsonCustomerInfo = json_decode($data);
                $customerId = getValue('id', self::custMasterTbl, "shop='$shop' and shopify_customer_id={$decodeJsonCustomerInfo->id}");
                if ($customerId == '') {
                    $insertdata = array(
                        'shopify_customer_id' => $decodeJsonCustomerInfo->id,
                        'first_name' => $decodeJsonCustomerInfo->first_name,
                        'last_name' => $decodeJsonCustomerInfo->last_name,
                        'email' => $decodeJsonCustomerInfo->email,
                        'phone' => $decodeJsonCustomerInfo->phone,
                        'tags' => $decodeJsonCustomerInfo->tags,
                        'state' => $decodeJsonCustomerInfo->state,
                        'note' => $decodeJsonCustomerInfo->note,
                        'verified_email' => $decodeJsonCustomerInfo->verified_email,
                        'is_subscription_active' => 0,
                        'shop' => $_GET['shop']
                    );
                    $productMasterId = self::addMasterCustomer($insertdata);
                }
            }
        }
    }

    public static function shopifyCustomerUpdateWebhook() {
        if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];

            $data = file_get_contents('php://input');

            $verified = verify_webhook($data, $hmac_header);

            if ($verified) {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

                $decodeJsonCustomerInfo = json_decode($data);

                #region - Update Product Master Info
                $updatedata = array(
                    'shopify_customer_id' => $decodeJsonCustomerInfo->id,
                    'first_name' => $decodeJsonCustomerInfo->first_name,
                    'last_name' => $decodeJsonCustomerInfo->last_name,
                    'email' => $decodeJsonCustomerInfo->email,
                    'phone' => $decodeJsonCustomerInfo->phone,
                    'tags' => $decodeJsonCustomerInfo->tags,
                    'state' => $decodeJsonCustomerInfo->state,
                    'note' => $decodeJsonCustomerInfo->note,
                    'verified_email' => $decodeJsonCustomerInfo->verified_email,
                    'shop' => $_GET['shop']
                );
                $masterProductId = self::updateCustomerMaster($updatedata, $decodeJsonCustomerInfo->id, $_GET['shop']);
                #endregion
            }
        }
    }

    public static function shopifyCustomerDeleteWebhook() {
        if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];

            $data = file_get_contents('php://input');

            $verified = verify_webhook($data, $hmac_header);

            if ($verified) {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

                $decodeJsonCustomerInfo = json_decode($data);

                #region - Delete Master Product Info
                $masterProductId = self::deleteMasterCustomer($decodeJsonCustomerInfo->id, $_GET['shop']);
                #endregion
            }
        }
    }

}
