<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class Collections {

    const colTable = "collections";
    const flTable = "filter_group";
    

    private static $db;

    /**
     * Content::__construct()
     * 
     * @return
     */
    function __construct() {
        self::$db = Registry::get("Database");
    }

    public function processFilterGroup() {
        Filter::checkPost('groups', 'Filter group is required');
        if (empty(Filter::$msgs)) {
            $data = array(
                'shop' => $_SESSION['shop'],
                'fl_groups' => $_POST['fl_groups']
            );
            if (!Filter::$id)
                $data['created_at'] = @date('Y-m-d');

            if (Filter::$id) {
                self::$db->update(self::flTable, $data, "id=" . Filter::$id);
                $lastid = Filter::$id;
            } else {
                $lastid = self::$db->insert(self::flTable, $data);
            }

            $message = (Filter::$id) ? 'Filter group saved' : 'Filter group saved';

            if (self::$db->affected()) {
                $json['type'] = 'success';
                $json['id'] = $lastid;
                $json['title'] = Core::$word->SUCCESS;
                $json['message'] = Core::$word->ET_UPDATED;
                return json_encode($json);
            } else {
                $json['type'] = 'warning';
                $json['id'] = $lastid;
                $json['title'] = Core::$word->ALERT;
                $json['message'] = Core::$word->SYSTEM_PROCCESS;
                return json_encode($json);
            }
        } else {
            $json['type'] = 'error';
            $json['id'] = Filter::$id;
            $json['title'] = Core::$word->SYSTEM_ERR;
            $json['message'] = Filter::msgSingleStatus();
            return json_encode($json);
        }
    }
    
    public static function fetchStoreCollections(){
        
    }

    public static function getGroupTagsOfCollection($group, $colid) {
        $sql = "select distinct tag from (
                Select 
                  title,collection_id,
                  SUBSTRING_INDEX(SUBSTRING_INDEX(tags, ',', numbers.n), ',', -1) tag
                from
                  (select 1 n union all
                   select 2 union all select 3 union all
                   select 4 union all select 5) numbers INNER JOIN  `products` 
                  on CHAR_LENGTH(`" . self::dbTable . "`.tags)
                     -CHAR_LENGTH(REPLACE(`" . self::dbTable . "`.tags, ',', ''))>=numbers.n-1
                where tags LIKE '%$group%' and find_in_set('$colid',collection_id)
                ) a 
                where a.tag like '$group%'";

        $tags = self::$db->fetch_all($sql, true);
        if (!empty($tags)) {
            return $tags;
        } else {
            return 0;
        }
    }

    public static function getTagProductCount($tag,$colid) {
        $sql = "select count(id) as prodCount from products where find_in_set('$tag',tags) and find_in_set('{$colid}',collection_id)";
        $prodCount = self::$db->first($sql, true);
        if (!empty($prodCount)) {
            return $prodCount['prodCount'];
        } else {
            return 0;
        }
    }

}
