<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class Shopauth {

    const authTable = "shopauth";

    public $logged_in = null;
    public $shop;
    public $shopid;
    public $shopToken;
    public static $shopSeceret = SHOPIFY_APP_SHARED_SECRET;
    private static $db;

    /**
     * Content::__construct()
     * 
     * @return
     */
    function __construct() {
        self::$db = Registry::get("Database");
        $this->startSession();
    }

    /**
     * Shopauth::startSession()
     * 
     * @return
     */
    private function startSession() {
        if (strlen(session_id()) < 1)
            session_start();

        $this->logged_in = $this->loginCheck();

        if (!$this->shop) {
            $this->shopToken = '';
        }
    }

    /**
     * Shopauth::loginCheck()
     * 
     * @return
     */
    private function loginCheck() {
        if (isset($_SESSION['shop']) && $_SESSION['shop'] != "") {

            $row = $this->getShopInfo($_SESSION['shop']);
            $this->shop = $row->shop_url;
            $this->shopid = $row->id;
            $this->shopToken = $row->access_token;
            return true;
        } else {
            return false;
        }
    }

    public function is_Shop() {
        return $this->shop;
    }

    /**
     * Shopauth::login()
     * 
     * @param mixed $shop
     * @return
     */
    public function login($shop) {
        if ($shop == "") {
            Filter::$msgs['shop'] = 'There is no shop found.';
        } else {
            $status = $this->checkStatus($shop);

            if (!$status) {
                Filter::$msgs['shop'] = 'There is no shop found.';
            }
        }
        if (empty(Filter::$msgs) && $status == true) {
            $row = $this->getShopInfo($shop);
            $this->shop = $_SESSION['shop'] = $row->shop_url;
            $this->shopid = $_SESSION['shopid'] = $row->id;
            $this->shopToken = $_SESSION['shopToken'] = $row->access_token;
            $_SESSION['filter_flag'] = $row->filter_flag;
            return true;
        } else
            Filter::msgStatus();
    }

    /**
     * Shopauth::getShopInfo()
     * 
     * @param mixed $shop
     * @return
     */
    public function getShopInfo($shop) {
        $shop_url = sanitize($shop);
        $shop_url = self::$db->escape($shop_url);

        if (!is_numeric($shop_url)) {
            $sql = "SELECT * FROM " . self::authTable . " WHERE shop_url ='" . $shop_url . "'";
        }

        $row = self::$db->first($sql);

        if (!$shop_url)
            return false;

        return ($row) ? $row : 0;
    }

    /**
     * Shopauth::checkStatus()
     * 
     * @param mixed $username
     * @param mixed $pass
     * @return
     */
    public function checkStatus($shop) {

        $shop_url = sanitize($shop);
        $shop_url = self::$db->escape($shop_url);
        $pass = sanitize($pass);

        $sql = "SELECT shop_url, access_token FROM " . self::authTable
                . "\n WHERE shop_url = '" . $shop_url . "'";
        $result = self::$db->query($sql);


        if (self::$db->numrows($result) == 0)
            return 0;

        $row = self::$db->fetch($result);

        if ($row->access_token != '') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Shopauth::logout()
     * 
     * @return
     */
    public function logout() {

        unset($_SESSION['shop']);
        unset($_SESSION['shopid']);
        unset($_SESSION['shoptoken']);
        unset($_SESSION['filter_flag']);

        session_destroy();
        session_regenerate_id();

        $this->logged_in = false;
        $this->shop = "";
        $this->shopid = '';
        $this->shopToken = "";
    }

    public static function getShippingMethod($store) {
        $shippingZones = $store->ShippingZone()->get();
        if (!empty($shippingZones) && is_array($shippingZones)) {
            $finalZones = array();
            foreach ($shippingZones as $shippingZone) {
                if (isset($shippingZone['price_based_shipping_rates']) && !empty($shippingZone['price_based_shipping_rates'])) {
                    foreach ($shippingZone['price_based_shipping_rates'] as $price) {
                        $finalZones['price_based'][] = $price;
                    }
                } elseif (isset($shippingZone['weight_based_shipping_rates']) && !empty($shippingZone['weight_based_shipping_rates'])) {
                    foreach ($shippingZone['weight_based_shipping_rates'] as $weight) {
                        $finalZones['weight_based'][] = $weight;
                    }
                } elseif (isset($shippingZone['carrier_shipping_rate_providers']) && !empty($shippingZone['carrier_shipping_rate_providers'])) {
                    foreach ($shippingZone['carrier_shipping_rate_providers'] as $carrier) {
                        $finalZones['carrier_based'][] = $carrier;
                    }
                }
            }
            return $finalZones;
        }
    }

    public static function uninstallWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . 'uninstall.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);

        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $uninstallWebhook = array();
        $uninstallWebhook = array(
            'topic' => 'app/uninstalled',
            'address' => SITEURL . 'uninstall.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($uninstallWebhook);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    

    public static function getWebhooks($store, $shop) {
        return $existWebhooks = $store->Webhook->get();
    }

    public static function deleteWebhook($store, $id) {
        @$store->Webhook($id)->delete();
    }

    public static function orderCreateWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/order-create.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'orders/create',
            'address' => SITEURL . '/order-create.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function productCreateWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/product-create.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'products/create',
            'address' => SITEURL . '/product-create.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function productUpdateWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/product-update.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'products/update',
            'address' => SITEURL . '/product-update.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function productDeleteWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/product-delete.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'products/delete',
            'address' => SITEURL . '/product-delete.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function customerCreateWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/customer-create.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'customers/create',
            'address' => SITEURL . '/customer-create.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function customerUpdateWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/customer-update.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'customers/update',
            'address' => SITEURL . '/customer-update.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    public static function customerDeleteWebhook($store, $shop) {

        $params = array(
            'address' => SITEURL . '/customer-delete.php?shop=' . $shop
        );
        $existWebhook = $store->Webhook->get($params);
        if (!empty($existWebhook)) {
            foreach ($existWebhook as $exwebhook) {
                @$store->Webhook($exwebhook['id'])->delete();
            }
        }

        $webhookReq = array();
        $webhookReq = array(
            'topic' => 'customers/delete',
            'address' => SITEURL . '/customer-delete.php?shop=' . $shop,
            'format' => 'json'
        );

        try {
            $webhook = $store->Webhook->post($webhookReq);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

}
