<?php
/**
 * Created by PhpStorm.
 * @author Tareq Mahmood <tareqtms@yahoo.com>
 * Created at 8/19/16 2:28 PM UTC+06:00
 *
 * @see https://help.shopify.com/api/reference/discount Shopify API Reference for Discount
 */


namespace PHPShopify;

/*
 * --------------------------------------------------------------------------
 * DraftOrder -> Custom actions
 * --------------------------------------------------------------------------
 *
 */
class DraftOrder extends ShopifyResource
{
    /**
     * @inheritDoc
     */
    protected $resourceKey = 'draft_order';

    
    
}