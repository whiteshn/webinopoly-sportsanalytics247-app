<?php

/**
 * Minify Class
 *
 * @package CMS Pro
 * @author wojoscripts.com
 * @copyright 2014
 * @version $Id: class_minify.php, v1.00 2012-03-05 10:12:05 gewa Exp $
 */
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class Minify {

    const prefix = 'master_';
    const suffixcss = '.css';
    const suffixjs = '.js';

    /**
     * Minify::__callStatic()
     * 
     * @param mixed $type
     * @param mixed $source
     * @return
     */
    public static function cache($source, $type, $theme = THEME) {

        switch ($type) {
            case 'css':
                $suffix = self::suffixcss;
                break;
            case 'js':
                $suffix = self::suffixjs;
                break;
            default:
                $suffix = self::suffixcss;
                break;
        }

        $target = $theme . '/cache/';
        $last_change = self::last_change($source, $theme);

        $temp = $target . self::prefix . 'main' . $suffix;

        if (!file_exists($temp) || $last_change > filemtime($temp)) {
            if (!self::write_cache($source, $temp, $type, $theme)) {
                Filter::msgError("Minify:: - Writing the file to <{$target}> failed!");
            }
        }

        return basename($temp);
    }

    /**
     * Minify::last_change()
     * 
     * @param mixed $files
     * @return
     */
    protected static function last_change($files, $theme = THEME) {
        foreach ($files as $key => $file) {
            $files[$key] = filemtime($theme . '/' . $file);
        }

        sort($files);
        $files = array_reverse($files);

        return $files[key($files)];
    }

    /**
     * Minify::write_cache()
     * 
     * @param mixed $files
     * @param mixed $target
     * @param mixed $type
     * @return
     */
    protected static function write_cache($files, $target, $type, $theme = THEME) {

        $content = "";

        foreach ($files as $file) {
            $content .= file_get_contents($theme . '/' . $file);
        }


        $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
        $content = str_replace(array(
            "\r\n",
            "\r",
            "\n",
            "\t",
            '  ',
            '    ',
            '    '), '', $content);
        $content = str_replace(array(
            ': ',
            ' {',
            ';}'), array(
            ':',
            '{',
            '}'), $content);

        if (!file_exists($theme . '/cache/'))
            mkdir($theme . '/cache/');

        return file_put_contents($target, $content);
    }

}

?>