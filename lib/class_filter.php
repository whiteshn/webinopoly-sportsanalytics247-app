<?php

/**
 * Filter Class
 *
 * @package Freelance Manager
 * @author wojoscripts.com
 * @copyright 2010
 * @version $Id: class_filter.php, v1.00 2011-04-20 18:20:24 gewa Exp $
 */
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

final class Filter {

    public static $id = null;
    public static $get = array();
    public static $post = array();
    public static $cookie = array();
    public static $files = array();
    public static $server = array();
    private static $marker = array();
    public static $msgs = array();
    public static $showMsg;
    public static $action = null;
    public static $msg = null;
    public static $do = null;

    /**
     * Filter::__construct()
     * 
     * @return
     */
    public function __construct() {

        $_GET = self::clean($_GET);
        $_POST = self::clean($_POST);
        $_COOKIE = self::clean($_COOKIE);
        $_FILES = self::clean($_FILES);
        $_SERVER = self::clean($_SERVER);

        self::$get = $_GET;
        self::$post = $_POST;
        self::$cookie = $_COOKIE;
        self::$files = $_FILES;
        self::$server = $_SERVER;

        self::getAction();
        self::getDo();
        self::$id = self::getId();
        self::$msg = self::getMsg();
    }

    /**
     * Filter::getId()
     * 
     * @return
     */
    private static function getId() {
        if (isset($_REQUEST['id'])) {
            self::$id = (is_numeric($_REQUEST['id']) && $_REQUEST['id'] > -1) ? intval($_REQUEST['id']) : false;
            self::$id = sanitize(self::$id);

            if (self::$id == false) {
                DEBUG == true ? self::error("You have selected an Invalid Id", "Filter::getId()") : self::ooops();
            } else
                return self::$id;
        }
    }

    /**
     * Filter::getMsg()
     * 
     * @return
     */
    private static function getMsg() {
        if (isset($_REQUEST['msg'])) {
            self::$msg = (!is_numeric($_REQUEST['msg']) && $_REQUEST['msg'] != '') ? $_REQUEST['msg'] : '';
            self::$msg = sanitize(self::$msg);

            if (self::$msg == false) {
                DEBUG == true ? self::error("You have selected an Invalid msg", "Filter::getMsg()") : self::ooops();
            } else
                return self::$msg;
        }
    }

    /**
     * Filter::clean()
     * 
     * @param mixed $data
     * @return
     */
    public static function clean($data) {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                unset($data[$key]);

                $data[self::clean($key)] = self::clean($value);
            }
        } else {
            if (ini_get('magic_quotes_gpc')) {
                $data = stripslashes($data);
            } else {
                $data = htmlspecialchars($data, ENT_QUOTES, 'UTF-8');
            }
        }

        return $data;
    }

    /**
     * Filter::msgAlert()
     * 
     * @param mixed $msg
     * @param bool $fader
     * @param bool $print
     * @param bool $altholder
     * @return
     */
    public static function msgAlert($msg, $print = true, $fader = false, $altholder = false) {
        self::$showMsg = "<div class=\"wojo icon message warning\"><i class=\"flag icon\"></i><i class=\"close icon\"></i><div class=\"content\"><div class=\"header\"> " . Core::$word->ALERT . "</div><p>" . $msg . "</p></div></div>";
        if ($fader == true)
            self::$showMsg .= "<script type=\"text/javascript\"> 
		  // <![CDATA[
			setTimeout(function() {       
			  $(\".msgAlert\").fadeOut(\"slow\",    
			  function() {       
				$(\".msgAlert\").remove();  
			  });
			},
			4000);
		  // ]]>
		  </script>";

        if ($print == true) {
            print ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        } else {
            return ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        }
    }

    /**
     * Filter::msgSingleAlert()
     * 
     * @param mixed $msg
     * @param bool $print
     * @return
     */
    public static function msgSingleAlert($msg, $print = true) {
        self::$showMsg = '<div class="Polaris-Banner Polaris-Banner--statusWarning Polaris-Banner--hasDismiss" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner6Heading" aria-describedby="Banner6Content">         <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorYellowDark Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m0-13a1 1 0 0 0-1 1v4a1 1 0 1 0 2 0V6a1 1 0 0 0-1-1m0 8a1 1 0 1 0 0 2 1 1 0 0 0 0-2"></path></g></svg></span></div>         <div>             <div class="Polaris-Banner__Heading" id="Banner6Heading">                 <p class="Polaris-Heading">' . $msg . '</p>             </div>         </div>     </div>';

        if ($print == true) {
            print self::$showMsg;
        } else {
            return self::$showMsg;
        }
    }

    /**
     * Filter::msgOk()
     * 
     * @param mixed $msg
     * @param bool $fader
     * @param bool $print
     * @param bool $altholder
     * @return
     */
    public static function msgOk($msg, $print = true, $fader = false, $altholder = false) {
        self::$showMsg = "<div class=\"wojo icon message success\"><i class=\"flag icon\"></i><i class=\"close icon\"></i><div class=\"content\"><div class=\"header\"> " . Core::$word->SUCCESS . "</div><p>" . $msg .
                "</p></div></div>";
        if ($fader == true)
            self::$showMsg .= "<script type=\"text/javascript\"> 
		  // <![CDATA[
			setTimeout(function() {       
			  $(\".wojo.icon.message.success\").fadeOut(\"slow\",    
			  function() {       
				$(\".wojo.icon.message.success\").remove();  
			  });
			},
			4000);
		  // ]]>
		  </script>";
        if ($print == true) {
            print ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        } else {
            return ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        }
    }

    /**
     * Filter::msgSingleOk()
     * 
     * @param mixed $msg
     * @param bool $print
     * @return
     */
    public static function msgSingleOk($msg, $print = true) {
        self::$showMsg = '<div class="Polaris-Banner Polaris-Banner--statusSuccess Polaris-Banner--hasDismiss" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner6Heading" aria-describedby="Banner6Content">         <div class="Polaris-Banner__Ribbon">             <span class="Polaris-Icon Polaris-Icon--colorGreenDark Polaris-Icon--hasBackdrop">                 <svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8m2.293-10.707L9 10.586 7.707 9.293a1 1 0 1 0-1.414 1.414l2 2a.997.997 0 0 0 1.414 0l4-4a1 1 0 1 0-1.414-1.414"></path></g></svg>             </span>         </div>         <div>             <div class="Polaris-Banner__Heading" id="Banner6Heading">                 <p class="Polaris-Heading">' . $msg . '</p>             </div>         </div>     </div>';
        if ($print == true) {
            print self::$showMsg;
        } else {
            return self::$showMsg;
        }
    }

    /**
     * Filter::msgInfo()
     * 
     * @param mixed $msg
     * @param bool $fader
     * @param bool $print
     * @param bool $altholder
     * @return
     */
    public static function msgInfo($msg, $print = true, $fader = false, $altholder = false) {
        self::$showMsg = "<div class=\"wojo icon message info\"><i class=\"flag icon\"></i><i class=\"close icon\"></i><div class=\"content\"><div class=\"header\"> " . Core::$word->INFO . "</div><p>" . $msg . "</p></div></div>";
        if ($fader == true)
            self::$showMsg .= "<script type=\"text/javascript\"> 
		  // <![CDATA[
			setTimeout(function() {       
			  $(\".msgInfo\").fadeOut(\"slow\",    
			  function() {       
				$(\".msgInfo\").remove();  
			  });
			},
			4000);
		  // ]]>
		  </script>";

        if ($print == true) {
            print ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        } else {
            return ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        }
    }

    /**
     * Filter::msgSingleInfo()
     * 
     * @param mixed $msg
     * @param bool $print
     * @return
     */
    public static function msgSingleInfo($msg, $print = true) {
        self::$showMsg = "<div class=\"wojo info message\"><i class=\"information icon\"></i> " . $msg . "</div>";

        if ($print == true) {
            print self::$showMsg;
        } else {
            return self::$showMsg;
        }
    }

    /**
     * Filter::msgError()
     * 
     * @param mixed $msg
     * @param bool $fader
     * @param bool $print
     * @param bool $altholder
     * @return
     */
    public static function msgError($msg, $print = true, $fader = false, $altholder = false) {
        self::$showMsg = "<div class=\"wojo icon message error\"><i class=\"flag icon\"></i><i class=\"close icon\"></i><div class=\"content\"><div class=\"header\"> " . Core::$word->ERROR . "</div><p>" . $msg .
                "</p></div></div>";
        if ($fader == true)
            self::$showMsg .= "<script type=\"text/javascript\"> 
		  // <![CDATA[
			setTimeout(function() {       
			  $(\".msgError\").fadeOut(\"slow\",    
			  function() {       
				$(\".msgError\").remove();  
			  });
			},
			4000);
		  // ]]>
		  </script>";
        if ($print == true) {
            print ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        } else {
            return ($altholder) ? '<div id="alt-msgholder">' . self::$showMsg . '</div>' : self::$showMsg;
        }
    }

    /**
     * Filter::msgSingleError()
     * 
     * @param mixed $msg
     * @param bool $print
     * @return
     */
    public static function msgSingleError($msg, $print = true) {
        self::$showMsg = '<div class="Polaris-Banner Polaris-Banner--statusCritical Polaris-Banner--hasDismiss" tabindex="0" role="status" aria-live="polite" aria-labelledby="Banner6Heading" aria-describedby="Banner6Content">         <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorRedDark Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M2 10c0-1.846.635-3.543 1.688-4.897l11.209 11.209A7.954 7.954 0 0 1 10 18c-4.411 0-8-3.589-8-8m14.312 4.897L5.103 3.688A7.954 7.954 0 0 1 10 2c4.411 0 8 3.589 8 8a7.952 7.952 0 0 1-1.688 4.897M0 10c0 5.514 4.486 10 10 10s10-4.486 10-10S15.514 0 10 0 0 4.486 0 10"></path></g></svg></span></div>         <div>             <div class="Polaris-Banner__Heading" id="Banner6Heading">                 <p class="Polaris-Heading">' . $msg . '</p>             </div>         </div>     </div>';

        if ($print == true) {
            print self::$showMsg;
        } else {
            return self::$showMsg;
        }
    }

    /**
     * Filter::msgStatus()
     * 
     * @return
     */
    public static function msgStatus() {
        self::$showMsg = "<div class=\"wojo error message\" style='border-radius:10px;color:#fff!important'><i class=\"close icon\"></i><div class=\"header\">" . Core::$word->SYSTEM_ERR . "</div><div class=\"content\"><ul class=\"wojo list\">";
        $i = count(self::$showMsg);
        foreach (self::$msgs as $msg) {
            self::$showMsg .= "<li>" . $msg . "</li>\n";
        }
        self::$showMsg .= "</ul></div></div>";

        return self::$showMsg;
    }

    /**
     * Filter::msgSingleStatus()
     * 
     * @return
     */
    public static function msgSingleStatus() {
        self::$showMsg = "<ul>";
        $i = count(self::$showMsg);
        foreach (self::$msgs as $msg) {
            self::$showMsg .= "<li>" . $msg . "</li>\n";
        }
        self::$showMsg .= "</ul>";

        return self::$showMsg;
    }

    /**
     * Filter::error()
     * 
     * @param mixed $msg
     * @param mixed $source
     * @return
     */
    public static function error($msg, $source) {
        if (DEBUG == true) {
            $the_error = "<div class=\"red\">";
            $the_error .= "<span>System ERROR!</span><br />";
            $the_error .= "DB Error: " . $msg . " <br /> More Information: <br />";
            $the_error .= "<ul class=\"error\">";
            $the_error .= "<li> Date : " . date("F j, Y, g:i a") . "</li>";
            $the_error .= "<li> Function: " . $source . "</li>";
            $the_error .= "<li> Script: " . $_SERVER['REQUEST_URI'] . "</li>";
            $the_error .= "<li>&lsaquo; <a href=\"javascript:history.go(-1)\"><strong>Go Back to previous page</strong></a></li>";
            $the_error .= '</ul>';
            $the_error .= '</div>';
        } else {
            $the_error = "<div class=\"msgError\" style=\"color:#444;width:400px;margin-left:auto;margin-right:auto;border:1px solid #C3C3C3;font-family:Arial, Helvetica, sans-serif;font-size:13px;padding:10px;background:#f2f2f2;border-radius:5px;text-shadow:1px 1px 0 #fff\">";
            $the_error .= "<h4 style=\"font-size:18px;margin:0;padding:0\">Oops!!!</h4>";
            $the_error .= "<p>Something went wrong. Looks like the page you're looking for was moved or never existed. Make sure you typed the correct URL or followed a valid link.</p>";
            $the_error .= "<p>&lsaquo; <a href=\"javascript:history.go(-1)\" style=\"color:#0084FF;\"><strong>Go Back to previous page</strong></a></p>";
            $the_error .= '</div>';
        }
        print $the_error;
        die();
    }

    /**
     * Filter::ooops()
     * 
     * @return
     */
    public static function ooops() {
        $the_error = "<div class=\"red\" style=\"color:#444;width:400px;margin-left:auto;margin-right:auto;border:1px solid #C3C3C3;font-family:Arial, Helvetica, sans-serif;font-size:13px;padding:10px;background:#f2f2f2;border-radius:5px;text-shadow:1px 1px 0 #fff\">";
        $the_error .= "<h4 style=\"font-size:18px;margin:0;padding:0\">Oops!!!</h4>";
        $the_error .= "<p>Something went wrong. Looks like the page you're looking for was moved or never existed. Make sure you typed the correct URL or followed a valid link.</p>";
        $the_error .= "<p>&lsaquo; <a href=\"javascript:history.go(-1)\" style=\"color:#0084FF;\"><strong>Go Back to previous page</strong></a></p>";
        $the_error .= '</div>';
        print $the_error;
        die();
    }

    /**
     * Filter::getAction()
     * 
     * @return
     */
    private static function getAction() {
        if (isset(self::$get['action'])) {
            $action = ((string) self::$get['action']) ? (string) self::$get['action'] : false;
            $action = sanitize($action);

            if ($action == false) {
                self::error("You have selected an Invalid Action Method", "Filter::getAction()");
            } else
                return self::$action = $action;
        }
    }

    /**
     * Filter::getDo()
     * 
     * @return
     */
    private static function getDo() {
        if (isset(self::$get['do'])) {
            $do = ((string) self::$get['do']) ? (string) self::$get['do'] : false;
            $do = sanitize($do);

            if ($do == false) {
                self::error("You have selected an Invalid Do Method", "Filter::getDo()");
            } else
                return self::$do = $do;
        }
    }

    /**
     * Filter::checkPost()
     * 
     * @param mixed $index
     * @param mixed $msg
     * @return
     */
    public static function checkPost($index, $msg, $parentindex = '') {

        if ($parentindex != '') {
            if (empty($_POST[$parentindex][$index][$subindex]))
                self::$msgs[$index . '_' . $subindex] = $msg;
        } else {
            if (empty($_POST[$index]))
                self::$msgs[$index] = $msg;
        }
    }

    /**
     * Filter::checkNumeric()
     * 
     * @param mixed $index
     * @param mixed $msg
     * @return
     */
    public static function checkNumeric($index, $msg) {

        if (!empty($_POST[$index]) && !is_numeric($_POST[$index]))
            self::$msgs[$index] = $msg;
    }

    /**
     * Filter:checkValidEmail()
     * 
     * @param mixed $email
     * @return
     */
    public static function checkValidEmail($email) {

        if (function_exists('filter_var')) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                return true;
            } else {
                return false;
            }
        } else {
            return preg_match('/^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/', $email);
        }
    }

    /**
     * Filter::dodate()
     * 
     * @param mixed $format
     * @param mixed $date
     * @return
     */
    public static function dodate($format, $date) {

        return strftime(Registry::get("Core")->$format, strtotime($date));
    }

    /**
     * Filter::readFile()
     * 
     * @param mixed $filename
     * @param boll $retbytes
     * @return
     */
    public static function readFile($filename, $retbytes = true) {
        $chunksize = 1 * (1024 * 1024);
        $buffer = '';
        $cnt = 0;

        $handle = fopen($filename, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle)) {
            $buffer = fread($handle, $chunksize);
            echo $buffer;
            ob_flush();
            flush();
            if ($retbytes) {
                $cnt += strlen($buffer);
            }
        }
        $status = fclose($handle);
        if ($retbytes && $status) {
            return $cnt;
        }
        return $status;
    }

    /**
     * Filter::fetchFile()
     * 
     * @param mixed $dirname
     * @param mixed $fname
     * @param mixed $file_path
     * @return
     */
    public function fetchFile($dirname, $fname, &$file_path) {
        $dir = opendir($dirname);

        while ($file = readdir($dir)) {
            if (empty($file_path) && $file != '.' && $file != '..') {
                if (is_dir($dirname . '/' . $file)) {
                    self::fetchFile($dirname . '/' . $file, $fname, $file_path);
                } else {
                    if (file_exists($dirname . '/' . $fname)) {
                        $file_path = $dirname . '/' . $fname;
                        return;
                    }
                }
            }
        }
    }

    /**
     * Filter::mark()
     * 
     * @param mixed $name
     * @return
     */
    public static function mark($name) {
        self::$marker[$name] = microtime();
    }

    /**
     * Filter::elapsed()
     * 
     * @param string $point1
     * @param string $point2
     * @param integer $decimals
     * @return
     */
    public static function elapsed($point1 = '', $point2 = '', $decimals = 4) {

        if (!isset(self::$marker[$point1])) {
            return '';
        }

        if (!isset(self::$marker[$point2])) {
            self::$marker[$point2] = microtime();
        }

        list($sm, $ss) = explode(' ', self::$marker[$point1]);
        list($em, $es) = explode(' ', self::$marker[$point2]);

        return number_format(($em + $es) - ($sm + $ss), $decimals);
    }

}

?>