<?php

if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class Products {

    const ProductMasterTbl = "store_products_master";
    const ProductVariantTbl = "store_products_variants_master";

    private static $db;

    /**
     * Content::__construct()
     * 
     * @return
     */
    function __construct() {
        self::$db = Registry::get("Database");
    }
    
    public static function getPkgOptions($shop) {
        $sql = "select id,store_product_title from " . self::ProductMasterTbl." where shop='$shop'";
        $options = self::$db->fetch_all($sql, true);
        return $options;
    }

    protected static function addMasterProduct_f_mdl($insertdata) {
        $insertedId = self::$db->insert(self::ProductMasterTbl, $insertdata);
        return $insertedId;
    }

    protected static function bulkProductVariantsInsert_f_mdl($variantsBulkIns) {
        $insertedId = self::$db->query("INSERT INTO " . self::ProductVariantTbl . "(store_products_master_id, store_product_var_id, store_product_var_title, store_product_var_sku, store_product_var_img, store_product_var_price, created_on,shop) VALUES" . $variantsBulkIns);
    }

    protected static function updateProductMasterInfo_f_mdl($updatedata, $storeProductId, $shop) {
        $updatedId = self::$db->update(self::ProductMasterTbl, $updatedata, "store_product_id='$storeProductId' and shop='$shop'");
        return $updatedId;
    }

    protected static function updateProductVariantsInfo_f_mdl($masterProductId, $variantsBulkIns, $shop) {
        #region - Remove Old Product Variants Records
        $deleteVariants = self::$db->delete(self::ProductVariantTbl, "store_products_master_id=$masterProductId and shop='$shop'");
        #endregion
        #region - Add New Product Variants
        self::bulkProductVariantsInsert_f_mdl($variantsBulkIns);
        #endregion
    }

    protected static function deleteMasterProduct_f_mdl($productId, $shop) {
        $retunrMasterProductId = self::$db->delete(self::ProductMasterTbl, "store_product_id=$productId and shop='$shop'");
        return $retunrMasterProductId;
    }

    protected static function deleteProductVariants_f_mdl($masterProductId, $shop) {
        $deleteVariants = self::$db->delete(self::ProductVariantTbl, "store_products_master_id=$masterProductId and shop='$shop'");
    }

    public static function shopifyProductCreateWebhook() {
        if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
            $data = file_get_contents('php://input');
            $verified = verify_webhook($data, $hmac_header);

            if ($verified) {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

                $decodeJsonProductsInfo = json_decode($data);

                $masterProductsArray = array();

                #region - Fetch Product Info
                $innerArray = array();
                $innerArray["store_product_id"] = $decodeJsonProductsInfo->id;
                $innerArray["store_product_title"] = $decodeJsonProductsInfo->title;
                $innerArray["store_product_handle"] = $decodeJsonProductsInfo->handle;
                $innerArray["store_product_variants"] = $decodeJsonProductsInfo->variants;
                $innerArray["store_product_images"] = $decodeJsonProductsInfo->images;

                if (isset($decodeJsonProductsInfo->images[0])) {
                    $innerArray["store_product_feature_img"] = $decodeJsonProductsInfo->images[0]->src;
                } else {
                    $innerArray["store_product_feature_img"] = "";
                }

                $masterProductsArray[] = $innerArray;
                #endregion
                #region - Add Products To DB
                if (count($masterProductsArray) > 0) {
                    $productMasterId = 0;
                    foreach ($masterProductsArray as $objProductInfo) {
                        #region - Add Product To DB
                        $insertdata = array(
                            'store_product_id' => $objProductInfo["store_product_id"],
                            'store_product_title' => $objProductInfo["store_product_title"],
                            'store_product_handle' => $objProductInfo["store_product_handle"],
                            'store_product_feature_img' => $objProductInfo["store_product_feature_img"],
                            'shop' => $_GET['shop']
                        );
                        $productMasterId = self::addMasterProduct_f_mdl($insertdata);
                        #endregion
                        #region - Add Products Variants
                        $variantsBulkIns = "";
                        if (count($objProductInfo["store_product_variants"] > 0)) {
                            foreach ($objProductInfo["store_product_variants"] as $objVariantInfo) {
                                $tempImgId = $objVariantInfo->image_id;
                                $tempImg = "";

                                #region - Find Variant Image
                                if (!empty($tempImgId)) {
                                    if (count($objProductInfo["store_product_images"]) > 0) {
                                        foreach ($objProductInfo["store_product_images"] as $objImgInfo) {
                                            if ($objImgInfo->id == $tempImgId) {
                                                $tempImg = $objImgInfo->src;
                                            }
                                        }
                                    }
                                } else {
                                    $tempImg = $store_product_feature_img;
                                }
                                #endregion

                                $variantsBulkIns .= "(" . $productMasterId . ", '" . $objVariantInfo->id . "', '" . $objVariantInfo->title . "', '" . $objVariantInfo->sku . "', '" . $tempImg . "', " . $objVariantInfo->price . ", now(), '" . $_GET['shop'] . "'),";
                            }
                        }

                        $variantsBulkIns = trim($variantsBulkIns, ",");

                        self::bulkProductVariantsInsert_f_mdl($variantsBulkIns);
                        #endregion
                    }
                }
                #endregion
            }
        }
    }

    public static function shopifyProductUpdateWebhook() {
        if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];

            $data = file_get_contents('php://input');

            $verified = verify_webhook($data, $hmac_header);

            if ($verified) {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

                $objProductInfo = json_decode($data);

                #region - Update Product Master Info
                $updatedata = array(
                    'store_product_id' => $objProductInfo->id,
                    'store_product_title' => $objProductInfo->title,
                    'store_product_handle' => $objProductInfo->handle,
                    'store_product_feature_img' => isset($objProductInfo->images[0]) ? $objProductInfo->images[0]->src : "",
                    'shop' => $_GET['shop'],
                );
                $masterProductId = self::updateProductMasterInfo_f_mdl($updatedata, $objProductInfo->id, $_GET['shop']);
                #endregion
                #region - Update Product Variants Info
                $variantsBulkIns = "";
                if (count($objProductInfo->variants) > 0) {
                    foreach ($objProductInfo->variants as $objVariantInfo) {
                        $variantsBulkIns .= "(" . $masterProductId . ", '" . $objVariantInfo->id . "', '" . $objVariantInfo->title . "', '" . $objVariantInfo->price . "', '" . $objVariantInfo->sku . "', " . $objVariantInfo->position . ", '" . $_GET['shop'] . "'),";
                    }
                }

                $variantsBulkIns = trim($variantsBulkIns, ",");

                self::updateProductVariantsInfo_f_mdl($masterProductId, $variantsBulkIns, $_GET['shop']);
                #endregion
            }
        }
    }

    public static function shopifyProductDeleteWebhook() {
        if (isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])) {
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];

            $data = file_get_contents('php://input');

            $verified = verify_webhook($data, $hmac_header);

            if ($verified) {
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

                $productUpdateInfo = json_decode($data);

                #region - Delete Master Product Info
                $masterProductId = self::deleteMasterProduct_f_mdl($productUpdateInfo->id, $_GET['shop']);
                #endregion
                #region - Delete Product Variants Info
                self::deleteProductVariants_f_mdl($masterProductId, $_GET['shop']);
                #endregion
            }
        }
    }

}
