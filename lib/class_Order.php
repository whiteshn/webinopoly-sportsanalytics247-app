<?php

/**
 * Content Class
 *
 * @package Membership Manager Pro
 * @author wojoscripts.com
 * @copyright 2015
 * @version $Id: class_core.php, v3.00 2015-02-10 10:12:05 gewa Exp $
 */
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class Order {

    const oMasterTbl = "orders_master";
    const oLineItemTbl = "orders_line_items_master";

    private static $db;

    /**
     * SettingsSplitpayment::__construct()
     * 
     * @return
     */
    function __construct() {
        self::$db = Registry::get("Database");
    }

    protected static function addMasterCustomer($insertdata) {
        $insertedId = self::$db->insert(Customer::custMasterTbl, $insertdata);
        return $insertedId;
    }

    public static function getSubscriptions($shop, $custid, $sub_type) {
        $sql = "select id,product_title,DATE_FORMAT(subscription_start_date,'%m/%d/%Y') as subscription_start_date,DATE_FORMAT(subscription_end_date,'%m/%d/%Y') as subscription_end_date,is_subscription_active from " . self::oLineItemTbl . " where shop='$shop' and shopify_customer_id=$custid and is_subscription_active=$sub_type";
        $subscriptions = self::$db->fetch_all($sql, true);
        return $subscriptions;
    }

    protected static function addMasterOrderInfo_f_mdl($insertdata) {
        $insertedId = self::$db->insert(self::oMasterTbl, $insertdata);
        return $insertedId;
    }

    protected static function addOrderLineItems_f_mdl($bulkInsertLineItems) {
        self::$db->query("INSERT INTO " . self::oLineItemTbl . "(orders_master_id, line_item_id, variant_id, variant_title, variant_sku, variant_quantity, variant_product_id,shop,shopify_customer_id,product_title,subscription_start_date,subscription_end_date,is_subscription_active) VALUES" . $bulkInsertLineItems);
    }

    public static function shopifyOrderCreateWebhook()
	{
        if(isset($_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'])){
            $hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];

            $data = file_get_contents('php://input');

            $verified = verify_webhook($data, $hmac_header);

            if ($verified) {
                extract($_GET);
                $varStoreName = $_SERVER['HTTP_X_SHOPIFY_SHOP_DOMAIN'];

                $orderInfo = json_decode($data);
				
				#region - Static Order Response
				//$orderInfo = json_decode('{"id":793243123773,"email":"hitesh@webinopoly.co","closed_at":null,"created_at":"2018-11-26T06:33:44-05:00","updated_at":"2018-11-26T06:33:45-05:00","number":15,"note":null,"token":"bd35003937ffb43324a93797e2b595c9","gateway":"Cash on Delivery (COD)","test":false,"total_price":"249.99","subtotal_price":"249.99","total_weight":0,"total_tax":"0.00","taxes_included":false,"currency":"USD","financial_status":"pending","confirmed":true,"total_discounts":"0.00","total_line_items_price":"249.99","cart_token":"25e51b2142db10d89e2d32ec3a35c0a3","buyer_accepts_marketing":false,"name":"#1015","referring_site":"","landing_site":"\/services\/tracking\/admin?source=\/admin\/apps\/sportsanalytics247\/sportsanalytics247\/index.php","cancelled_at":null,"cancel_reason":null,"total_price_usd":"249.99","checkout_token":"168d3082043ede848ca52885ca23b645","reference":null,"user_id":null,"location_id":null,"source_identifier":null,"source_url":null,"processed_at":"2018-11-26T06:33:44-05:00","device_id":null,"phone":null,"customer_locale":"en","app_id":580111,"browser_ip":null,"landing_site_ref":null,"order_number":1015,"discount_applications":[],"discount_codes":[],"note_attributes":[],"payment_gateway_names":["Cash on Delivery (COD)"],"processing_method":"manual","checkout_id":6229335900221,"source_name":"web","fulfillment_status":null,"tax_lines":[],"tags":"","contact_email":"hitesh@webinopoly.co","order_status_url":"https:\/\/sportgamemail.myshopify.com\/7235993661\/orders\/bd35003937ffb43324a93797e2b595c9\/authenticate?key=6e720814e97ebd7f6a2c68ad147490ba","presentment_currency":"USD","total_line_items_price_set":{"shop_money":{"amount":"249.99","currency_code":"USD"},"presentment_money":{"amount":"249.99","currency_code":"USD"}},"total_discounts_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}},"total_shipping_price_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}},"subtotal_price_set":{"shop_money":{"amount":"249.99","currency_code":"USD"},"presentment_money":{"amount":"249.99","currency_code":"USD"}},"total_price_set":{"shop_money":{"amount":"249.99","currency_code":"USD"},"presentment_money":{"amount":"249.99","currency_code":"USD"}},"total_tax_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}},"total_tip_received":"0.0","line_items":[{"id":1962454089789,"variant_id":13344522108989,"title":"2nd Half Package","quantity":1,"price":"249.99","sku":"","variant_title":"Month","vendor":"Sports Analytics","fulfillment_service":"manual","product_id":1576670003261,"requires_shipping":false,"taxable":false,"gift_card":false,"name":"2nd Half Package - Month","variant_inventory_management":null,"properties":[],"product_exists":true,"fulfillable_quantity":1,"grams":0,"total_discount":"0.00","fulfillment_status":null,"price_set":{"shop_money":{"amount":"249.99","currency_code":"USD"},"presentment_money":{"amount":"249.99","currency_code":"USD"}},"total_discount_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}},"discount_allocations":[],"tax_lines":[{"title":"TX State Tax","price":"0.00","rate":0.0625,"price_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}}},{"title":"Houston Municipal Tax","price":"0.00","rate":0.02,"price_set":{"shop_money":{"amount":"0.00","currency_code":"USD"},"presentment_money":{"amount":"0.00","currency_code":"USD"}}}],"origin_location":{"id":655424782397,"country_code":"US","province_code":"TX","name":"sportgamemail","address1":"7324 Southwest Fwy","address2":"","city":"Houston","zip":"77047"}}],"shipping_lines":[],"billing_address":{"first_name":"Hitesh","address1":"123 Broadway St.","phone":null,"city":"Houston","zip":"78754","province":"Texas","country":"United States","last_name":"Nimavat","address2":"","company":null,"latitude":null,"longitude":null,"name":"Hitesh Nimavat","country_code":"US","province_code":"TX"},"fulfillments":[],"client_details":{"browser_ip":"122.170.156.239","accept_language":"en-US,en;q=0.9","user_agent":"Mozilla\/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/70.0.3538.102 Safari\/537.36","session_hash":"07c29aff3cdd431458e09fb427e521cc","browser_width":1366,"browser_height":626},"refunds":[],"customer":{"id":852825210941,"email":"hitesh@webinopoly.co","accepts_marketing":false,"created_at":"2018-11-17T14:59:24-05:00","updated_at":"2018-11-26T06:33:45-05:00","first_name":"Hitesh","last_name":"Nimavat","orders_count":2,"state":"disabled","total_spent":"0.00","last_order_id":793243123773,"note":null,"verified_email":true,"multipass_identifier":null,"tax_exempt":false,"phone":null,"tags":"","last_order_name":"#1015","currency":"USD","default_address":{"id":1152776798269,"customer_id":852825210941,"first_name":"Hitesh","last_name":"Nimavat","company":null,"address1":"123 Broadway St.","address2":"","city":"Houston","province":"Texas","country":"United States","zip":"78754","phone":null,"name":"Hitesh Nimavat","province_code":"TX","country_code":"US","country_name":"United States","default":true}}}');
				#endregion

                #region - Get Order Master Info & Add To DB
                $order_id = $orderInfo->id;
                $order_no = $orderInfo->order_number;
                $order_email = $orderInfo->email;
				
				if(isset($orderInfo->shipping_lines) && count($orderInfo->shipping_lines) > 0){
					$order_shipping_charge = $orderInfo->shipping_lines[0]->price;
				}
				else{
					$order_shipping_charge = "0";
				}

                $order_customer_name = "N/A";
                if (array_key_exists("customer", $orderInfo)) {
                    $order_customer_name = $orderInfo->customer->first_name . " " . $orderInfo->customer->last_name;
                }

                $is_order_free = 0;
                if (ceil($order_shipping_charge) <= 0) {
                    $is_order_free = 1;
                    $order_shipping_charge = 0;
                }

                $order_customer_address1 = "";
                $order_customer_address2 = "";
                $order_customer_city = "";
                $order_customer_province = "";
                $order_customer_country = "";
                $order_customer_zip = "";
                $order_shipping_firstname = "";
                $order_shipping_lastname = "";
                if (array_key_exists("shipping_address", $orderInfo)) {
                    $order_customer_address1 = $orderInfo->shipping_address->address1;
                    $order_customer_address2 = $orderInfo->shipping_address->address2;
                    $order_customer_city = $orderInfo->shipping_address->city;
                    $order_customer_province = $orderInfo->shipping_address->province;
                    $order_customer_country = $orderInfo->shipping_address->country;
                    $order_customer_zip = $orderInfo->shipping_address->zip;

                    $order_shipping_firstname = $orderInfo->shipping_address->first_name;
                    $order_shipping_lastname = $orderInfo->shipping_address->last_name;
                }

                $insertdata = array(
                    'order_id' => $order_id,
                    'order_no' => $order_no,
                    'order_email' => $order_email,
                    'order_customer_name' => $order_customer_name,
                    'is_order_free' => $is_order_free,
                    'order_shipping_charge' => $order_shipping_charge,
                    'customer_address1' => $order_customer_address1,
                    'customer_address2' => $order_customer_address2,
                    'city' => $order_customer_city,
                    'province' => $order_customer_province,
                    'country' => $order_customer_country,
                    'zip' => $order_customer_zip,
                    'order_shipping_firstname' => $order_shipping_firstname,
                    'order_shipping_lastname' => $order_shipping_lastname,
                    'shop' => $_GET['shop']
                );
	
                $masterOrderId = self::addMasterOrderInfo_f_mdl($insertdata);
                #endregion
				
                #region - Get Order Line Items Info & Add To DB
                if (isset($orderInfo->line_items)) {
                    $bulkInsertLineItems = "";

                    foreach ($orderInfo->line_items as $objLineItems) {
                        $productNames = explode('-', $objLineItems->name);
                        array_pop($productNames);
                        $line_item_id = $objLineItems->id;
                        $variant_id = $objLineItems->variant_id;
                        $variant_title = $objLineItems->variant_title;
                        $variant_sku = $objLineItems->sku;
                        $variant_quantity = $objLineItems->quantity;
                        $variant_product_id = $objLineItems->product_id;
                        $shopify_customer_id = $orderInfo->customer->id;
                        $product_title = implode(' ', $productNames);

                        #region - Get Subscription ID Base On Store Product ID
						$subscriptionId = getValue('id', Products::ProductMasterTbl, "store_product_id='$variant_product_id' and shop='{$shop}'");
						#endregion
						
						#region - Get Game Mail Templates & Dates
						$gmailTpl = Registry::get("Core")->getRowByColumn(GameEmail::eTable, "subscription_id=$subscriptionId and sub_date='".@date('Y-m-d')."' and shop='{$shop}'", false, false);
						#endregion                        
                       
                        $startDate = '';
                        $endDate = '';
                        $is_subscription_active = 0;
                        $customerEnroll = false;
						
                        if(is_object($gmailTpl) && !empty($gmailTpl)) {
							#region - Get Order Created Date & Set It As UTC
							$orderCreateDate = $orderInfo->created_at;
							
							$splitDate = explode('T', $orderCreateDate);
							$finalOrderDateTime = '';
                            $finaltimzoneHour = '';
                            $finaltimzoneMin = '';
							
							if (strpos($splitDate[1], '+') !== false) {
                                $splitTimezone = explode('+', $splitDate[1]);
                                $finaltimzoneHour = "+" . explode(':', $splitTimezone[1])[0];
                                $finaltimzoneMin = $splitTimezone[1][1];
                            } elseif (strpos($splitDate[1], '-') !== false) {
                                $splitTimezone = explode('-', $splitDate[1]);
                                $finaltimzoneHour = "-" . explode(':', $splitTimezone[1])[0];
                                $finaltimzoneMin = $splitTimezone[1][1];
                            }
							#endregion
							
							#region - Get & Set Order Cut Off Date Time
							date_default_timezone_set('US/Eastern');
							
							$finalOrderDateTime = $splitDate[0] . ' ' . $splitTimezone[0];							
							$expireTime = str_replace(' am', ':00', str_replace(' pm', ':00', $gmailTpl->expiration_time));
							$cutofDateTime = new DateTime($gmailTpl->sub_date . ' ' . $expireTime);
							
							$currntDateTime = new DateTime($finalOrderDateTime);							
							$currntDateTime->modify("$finaltimzoneHour hour $finaltimzoneMin minutes");
							
							$objStartDate = new DateTime();
                            $objEndDate = new DateTime();
                            if ($currntDateTime > $cutofDateTime) {
                                $objStartDate->modify('+1 day');
                                $objEndDate->modify('+1 day');
                            }
							
                            if (strtolower($variant_title) == 'season') {
                                $objEndDate->modify('+59 day');
                            } elseif (strtolower($variant_title) == 'month') {
                                $objEndDate->modify('+29 day');
                            } elseif (strtolower($variant_title) == 'week') {
                                $objEndDate->modify('+6 day');
                            } elseif (strtolower($variant_title) == 'sunday only') {
                                $objEndDate->modify('next sunday');
                            }
							
                            $startDate = $objStartDate->format('Y-m-d H:i:s');
                            $endDate = $objEndDate->format('Y-m-d H:i:s');
                            $is_subscription_active = 1;
                            $customerEnroll = true;
							#endregion
                        }
						else{
							#region - Set Logic For Game Email Not Found
                            // if game mail not found
							#endregion
                        }
						
                        $bulkInsertLineItems .= "(" . $masterOrderId . ", '" . $line_item_id . "', '" . $variant_id . "', '" . $variant_title . "', '" . $variant_sku . "', " . $variant_quantity . ", '" . $variant_product_id . "', '" . $_GET['shop'] . "', '" . $shopify_customer_id . "', '" . $product_title . "', '" . $startDate . "', '" . $endDate . "',$is_subscription_active),";
                    }

                    if ($bulkInsertLineItems != "") {
                        $bulkInsertLineItems = trim($bulkInsertLineItems, ",");
						
                        self::addOrderLineItems_f_mdl($bulkInsertLineItems);
						
                        if($customerEnroll){
                            $customerId = getValue('id', Customer::custMasterTbl, "shop='$shop' and shopify_customer_id={$orderInfo->customer->id}");
							
                            if($customerId != ''){
                                self::$db->update(Customer::custMasterTbl, array('is_subscription_active' => 1), "shop='$shop' and shopify_customer_id={$orderInfo->customer->id}");
                            }
							else{
                                $decodeJsonCustomerInfo = $orderInfo->customer;
                                $insertdata = array(
                                    'shopify_customer_id' => $decodeJsonCustomerInfo->id,
                                    'first_name' => $decodeJsonCustomerInfo->first_name,
                                    'last_name' => $decodeJsonCustomerInfo->last_name,
                                    'email' => $decodeJsonCustomerInfo->email,
                                    'phone' => $decodeJsonCustomerInfo->phone,
                                    'tags' => $decodeJsonCustomerInfo->tags,
                                    'state' => $decodeJsonCustomerInfo->state,
                                    'note' => $decodeJsonCustomerInfo->note,
                                    'verified_email' => $decodeJsonCustomerInfo->verified_email,
                                    'is_subscription_active' => 1,
                                    'shop' => $shop
                                );
                                $productMasterId = self::addMasterCustomer($insertdata);
                            }
                        }
                    }
                }
                #endregion
            }
        }
    }

}