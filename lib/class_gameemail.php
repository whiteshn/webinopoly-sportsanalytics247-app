<?php

/**
 * Content Class
 *
 * @package Membership Manager Pro
 * @author wojoscripts.com
 * @copyright 2015
 * @version $Id: class_core.php, v3.00 2015-02-10 10:12:05 gewa Exp $
 */
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');

class GameEmail {

    const eTable = "game_email";

    private static $db;

    /**
     * Content::__construct()
     * 
     * @return
     */
    function __construct() {
        self::$db = Registry::get("Database");
    }

    /**
     * Content::getEmailTemplates()
     * 
     * @return
     */
    public static function getGameEmailTemplates($shop) {
        $sql = "SELECT gmt.id,gmt.subscription_id,gmt.subscription_title,gmt.title,DATE_FORMAT(gmt.sub_date,'%m/%d/%Y') as sub_date,gmt.expiration_time as expiration_time,gmt.send_email_time as send_email_time,gmt.is_mail_sent,pmt.store_product_feature_img,gmt.shop 
                FROM " . self::eTable . " gmt LEFT JOIN ".Products::ProductMasterTbl." pmt on gmt.subscription_id = pmt.id where gmt.shop='$shop' ORDER BY gmt.id DESC";
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : 0;
    }

    /**
     * Content:::processEmailTemplate()
     * 
     * @return
     */
    public function processGameEmail() {
		
		#region - Make Email Details Text Proper
		$emailDetails = "";

		if($_POST['email_details'] != ""){
			$emailDetailSplit = explode(PHP_EOL, $_POST['email_details']);
			$isFirst = true;
			foreach($emailDetailSplit as $val){
				if($isFirst){
					$emailDetails .= trim($val);
					$isFirst = false;
				}
				else
					$emailDetails .= "##BREAKLINE##".trim($val);
			}
		}
		#endregion
        
        $data = array(
            'subscription_id' => sanitize($_POST['subscription_id']),
            'subscription_title' => sanitize($_POST['subscription_title']),
            'sub_date' => @date('Y-m-d', strtotime($_POST['date'])),
            'title' => sanitize($_POST['title']),
            'expiration_time' => $_POST['expiration_time'],
            'comments' => sanitize($_POST['comments']),
            'email_details' => $emailDetails,
            'send_email_time' => $_POST['send_email_time'],
            'shop' => sanitize($_POST['shop'])
        );

        if (Filter::$id) {
            self::$db->update(self::eTable, $data, "id=" . Filter::$id);
            $last_id = Filter::$id;
            $msg = 'Game email updated successfully';
        } else {
            $last_id = self::$db->insert(self::eTable, $data);
            $msg = 'Game email added Successfully';
        }

        if (self::$db->affected()) {
            $json['type'] = 'success';
            $json['title'] = Core::$word->SUCCESS;
            $json['message'] = $msg;
            return json_encode($json);
        } else {
            $json['type'] = 'warning';
            $json['title'] = Core::$word->ALERT;
            $json['message'] = Core::$word->SYSTEM_PROCCESS;
            return json_encode($json);
        }
    }
	
	#region - Cron Job Methods
	public static function getGameEmailTempaltesBaseOnDateCondition($qryCondition)
	{
        $sql = "SELECT * FROM ".self::eTable." WHERE ".$qryCondition;
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : 0;
    }
	
	public static function setGameEmailTemplateSent($masterDBId)
	{
		$updateData = array(
            'is_mail_sent' => 1
        );
		
        self::$db->update(self::eTable, $updateData, "id=".$masterDBId);
    }
	
	public static function getGameEmailTemplateActiveSubscriber($subscriptionID)
	{
        $sql = "SELECT olim.id as orders_line_items_master_id, olim.subscription_start_date, olim.subscription_end_date, olim.game_mail_template_send_datetime, scm.email, scm.first_name, scm.last_name FROM store_products_master spm INNER JOIN orders_line_items_master olim ON spm.store_product_id = olim.variant_product_id AND olim.is_subscription_active = 1 INNER JOIN store_customer_master scm ON olim.shopify_customer_id = scm.shopify_customer_id WHERE spm.id = ".$subscriptionID;
		
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : array();
    }
	
	public static function sendGameMailTemplate($subscriptionTitle, $subscriptionEmailDetails, $subscriberEmail, $subscriberName)
	{
		$email = new \SendGrid\Mail\Mail(); 
		$email->setFrom("moe@webinopoly.com", "SportsAnalytics247");
		$tos = [ 
			$subscriberEmail => $subscriberName
		];
		$email->addTos($tos);
		$email->setSubject($subscriptionTitle);
		$email->addContent("text/html", self::mailBodyFormat($subscriptionEmailDetails));
		
		$sendgrid = new \SendGrid(SEND_GRID_API_KEY);
		try{
			$response = $sendgrid->send($email);
		}
		catch(Exception $e){
			print_r($e);
			exit;
		}
	}
	
	public static function mailBodyFormat($bodyContent)
	{
		return '<table width="100%" cellpadding="0" cellspacing="0" border="0" id="background-table" style="table-layout:fixed" align="center">
			<tbody><tr>
				<td align="center" bgcolor="#ececec">
					<table class="w640" style="margin:0 10px;" width="640" cellpadding="0" cellspacing="0" border="0">
						<tbody>
						<tr><td class="w640" width="640" bgcolor="#ececec" height="40"></td></tr>
						<tr><td class="w640" width="640" bgcolor="#ffffff" height="40"></td></tr>
						<tr>
						<td id="header" class="w640" width="640" align="center" bgcolor="#ffffff" style="margin-left:20px;">

			<div align="left" style="text-align:left; margin-left:40px; margin-right:40px">
				<a href="#"><img id="customHeaderImage" label="Header Image" editable="true" src="https://cdn.shopify.com/s/files/1/1918/1293/files/analytics2_250x.png?v=1533391780" class="w640" border="0" align="top" style="display: inline;width: 111px;"></a>
			</div>
		</td></tr>
						<tr><td class="w640" width="640" height="40" bgcolor="#ffffff"></td></tr>
						<tr id="simple-content-row"><td class="w640" width="640" bgcolor="#ffffff">
			<table align="left" class="w640" width="640" cellpadding="0" cellspacing="0" border="0">
				<tbody><tr>
					<td class="w30" width="40"></td>
					<td class="w580" width="560">
						<table class="w580" width="560" cellpadding="0" cellspacing="0" border="0">
							<tbody><tr><td width="560" height="30" style="vertical-align:top; font-family:Arial, Helvetica, sans-serif; font-size:16px; line-height:24px; color: #111"> '.$bodyContent.'<br/><br/></td></tr>
						</tbody></table>
					</td>
					<td class="w30" width="40"></td>
				</tr>
			</tbody></table>
		</td></tr>
						<tr>
						<td class="w640" width="640">
			<table width="640" style="height:98px" cellpadding="0" cellspacing="0" border="0" bgcolor="#0F0F0F">
							<tbody>
							<tr>
								<td width="40" height="45"></td>
								<td width="128" height="45">
								</td>
								<td width="100%" style="vertical-align:middle; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:16px; text-align:center" height="45">
								<p><span style="color:#ccc; margin-left: 25px;">&copy; '.date('Y').' <a style="color:#ccc;text-decoration: none;border-bottom: 1px dotted #3e3e3e;" href="#">SportsAnalytics247</a>. All Rights Reserved.</span></p>
								</td>
							</tr>
						</tbody></table>
						</td>
						</tr>
						<tr><td class="w580" width="640" height="40"></td></tr>
					</tbody></table>
				</td>
			</tr>
		</tbody></table>';
	}
	
	public static function updateMailSendDateTimeForParticularIds($masterIds)
	{
		$updateData = array(
            'game_mail_template_send_datetime' => 'now()'
        );
		
        self::$db->update("orders_line_items_master", $updateData, "id in(".$masterIds.")");
    }
	
	public static function setSubscriptionArchiveForOrderLineItems($masterIds)
	{
		$updateData = array(
            'is_subscription_active' => 0
        );
		
        self::$db->update("orders_line_items_master", $updateData, "id in(".$masterIds.")");
    }
	
	public static function getActiveSubscriptionCustomers()
	{
		$sql = "SELECT id as master_customer_id, shopify_customer_id FROM store_customer_master WHERE is_subscription_active = 1";
		
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : array();
	}
	
	public static function getActiveSubscriptionsByCustIDS($shopifyCustomerIds)
	{
		$sql = "SELECT shopify_customer_id FROM orders_line_items_master WHERE shopify_customer_id IN (".$shopifyCustomerIds.")";
		
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : array();
	}
	
	public static function setCustomersSubscriptionsArchive($customerMasterIds)
	{
		$updateData = array(
            'is_subscription_active' => 0
        );
		
        self::$db->update("store_customer_master", $updateData, "id in(".$customerMasterIds.")");
	}

	public static function getSpecificSubscription($shop, $subscriptionId)
	{
		$sql = "SELECT id, subscription_title FROM game_email WHERE id = ".$subscriptionId." and shop = '".$shop."'";
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : array();
	}

	public static function sendSubscriptionMailNow($shop, $subscriptionId)
	{
		#region - Set UTC Time Zone
		date_default_timezone_set('US/Eastern');
		$objCurDateTime = new DateTime();
		#endregion

		#region - Get Subscription Info
		$gameEmailsTemplates = self::getSpecificInstantGameEmailTempaltesBaseOnId($subscriptionId);
		#endregion

		if(count($gameEmailsTemplates) > 0){
			#region - Loop & Process Through Dates
			foreach($gameEmailsTemplates as $objTemplateInfo){
				#region - Get Game Email Templates Dates
				$masterGameEmailId = $objTemplateInfo->id;
				$subscriptionID = $objTemplateInfo->subscription_id;
				$subscriptionTitle = $objTemplateInfo->title;
				$subscriptionEmailDetails = $objTemplateInfo->email_details;
				$subscriptionDate = $objTemplateInfo->sub_date;
				$templateExpirationTime = $objTemplateInfo->expiration_time;
				$templateMailSendTime = $objTemplateInfo->send_email_time;
				#endregion
				
				#region - Set Game Email Template Dates Object
				$objTmplExprireDateTime = new DateTime($subscriptionDate." ".str_replace(" am", ":00", str_replace(" pm", ":00", $templateExpirationTime)));
				$objTmplSendMailDateTime = new DateTime($subscriptionDate." ".str_replace(" am", ":00", str_replace(" pm", ":00", $templateMailSendTime)));
				#endregion
				
				#region - Get Active Subscriptions & Set Game Mail Templates Expire If Any
				if($objCurDateTime < $objTmplExprireDateTime && $objCurDateTime >= $objTmplSendMailDateTime){
					#region - Fetch Active Subscribers List
					$activeSubscribersInfo = self::getGameEmailTemplateActiveSubscriber($subscriptionID);
					#endregion
					
					if(count($activeSubscribersInfo) > 0){
						$subscriberEmailsArray = array();
						$subscriptionEndIdsArray = array();
						
						#region - Loop & Get Required Info
						foreach($activeSubscribersInfo as $objSubscrInfo){
							#region - Get & Set Required Variables
							$orderLineItemsMasterId = $objSubscrInfo->orders_line_items_master_id;
							$orderSubscriptionStartDate = $objSubscrInfo->subscription_start_date;
							$orderSubscriptionEndDate = $objSubscrInfo->subscription_end_date;
							$orderSubscriberEmail = $objSubscrInfo->email;
							$ordersubscriberName = $objSubscrInfo->first_name." ".$objSubscrInfo->last_name;
							$gameMailTemplateSendDateTime = $objSubscrInfo->game_mail_template_send_datetime;
							#endregion
							
							#region - Check Subscription End Or Not
							$objSubscriptionStartDate = new DateTime($orderSubscriptionStartDate);
							$objSubscriptionEndDate = new DateTime($orderSubscriptionEndDate);
							$objGameMailSendDateTime = new DateTime($gameMailTemplateSendDateTime);
							
							if($objSubscriptionStartDate <= $objCurDateTime && $objSubscriptionEndDate >= $objCurDateTime){
								
								#region - Temp Mail Send Check Variables
								$objTempCurrentDateTime = new DateTime($objCurDateTime->format("Y-m-d"));
								$objTempMailSendDateTime = new DateTime($objGameMailSendDateTime->format("Y-m-d"));
								#endregion
								
								if($objTempMailSendDateTime < $objTempCurrentDateTime){
									#region - Collect Subscriber Email
									$tempInnerArray = array();
									$tempInnerArray["c_email"] = $orderSubscriberEmail;
									$tempInnerArray["c_name"] = $ordersubscriberName;
									$tempInnerArray["master_id"] = $orderLineItemsMasterId;
									
									$subscriberEmailsArray[] = $tempInnerArray;
									#endregion
								}
							}
							else{
								#region - Collect Subscription End ID
								$subscriptionEndIdsArray[] = $orderLineItemsMasterId;
								#endregion
							}
							#endregion
						}
						#endregion
						
						#region - Send Game Template To Subscriber
						if(count($subscriberEmailsArray) > 0){
							$updateIdsArray = array();
							foreach($subscriberEmailsArray as $objSubscriberInfo){
								self::sendGameMailTemplate($subscriptionTitle, str_replace("##BREAKLINE##", "<br/>", $subscriptionEmailDetails), $objSubscriberInfo["c_email"], $objSubscriberInfo["c_name"]);
								
								$updateIdsArray[] = $objSubscriberInfo["master_id"];
							}
							
							#region - Set Mail Send DateTime To Order Line Items
							self::updateMailSendDateTimeForParticularIds(implode(",", $updateIdsArray));
							#endregion
						}
						#endregion
						
						#region - Set Subscription Archive
						if(count($subscriptionEndIdsArray) > 0){
							#region - Set Subscription Archive To Order Line Items
							self::setSubscriptionArchiveForOrderLineItems(implode(",", $subscriptionEndIdsArray));
							#endregion
						}
						#endregion
					}
				}
				else if($objCurDateTime > $objTmplExprireDateTime){
					#region - Set Current Template Sent
					self::setGameEmailTemplateSent($masterGameEmailId);
					#endregion
				}
				#endregion
			}
			#endregion
		}
	}

	public static function getSpecificInstantGameEmailTempaltesBaseOnId($subscriptionId)
	{
		$sql = "SELECT * FROM ".self::eTable." WHERE id = ".$subscriptionId;
        $row = Registry::get("Database")->fetch_all($sql);

        return ($row) ? $row : array();
	}
	#endregion

}
