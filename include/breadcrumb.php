<?php

switch (Filter::$do) {
    case 'subscription':
        $customerFName = getValue('first_name', Customer::custMasterTbl, "shopify_customer_id={$_GET['cust_id']}");
        $customerLName = getValue('last_name', Customer::custMasterTbl, "shopify_customer_id={$_GET['cust_id']}");
        echo "title: 'Subscription',";
        echo "breadcrumb: {
                label: 'Customers - $customerFName $customerLName',
                href: 'index.php',
                target: 'app',
                loading: false
            }";
        break;
    case 'gameemail-add':
        echo "title: 'Add game email',";
        echo "breadcrumb: {
                label: 'Game Emails',
                href: 'index.php?do=gamemail',
                target: 'app',
                loading: false
            }";
        break;
    case 'gamemail':
        echo "title: 'Game Emails',";
        break;
    default:
        echo "title: 'Customers',";
        break;
} 
    
