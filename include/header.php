<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Customer Wholesale Pricing | Webinopoly Inc.">
        <meta name="author" content="Webinopoly Inc.">
        <meta name="keyword" content="customer pricing,wholesale pricing,dicsount">
        <link rel="shortcut icon" href="img/favicon.png">

        <title> Sports Analytics 24/7 | Webinopoly Inc.</title>

        <link rel="stylesheet" href="https://sdks.shopifycdn.com/polaris/3.0.1/polaris.min.css" />
        <style type="text/css">
            #subscription .Polaris-DataTable__Cell--header{
                font-weight: bold;
            }
            #error-banner .Polaris-ExceptionList .Polaris-ExceptionList__Item .Polaris-ExceptionList__Icon .Polaris-Icon{
               fill: #bf0711;
            }
            #game-email-form .Polaris-FormLayout .Polaris-FormLayout__Item:nth-child(6), #game-email-form .Polaris-FormLayout .Polaris-FormLayout__Item:nth-child(15){
                margin-top: 0px;
            }
            .manage-child-elements{
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
                align-items: center;
                flex-direction: row;
            }
            .manage-child-elements h3{
                flex-grow: 1;
            }

            @media only screen and (max-width: 768px) {
                .manage-child-elements{
                    flex-direction: column;
                }
                .manage-child-elements button{
                    margin-top: 10px;
                }
            }
        </style>


        <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
          <script src="js/lte-ie7.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var SITEURL = "<?= SITEURL; ?>";
            var SHOPIFY_APP_API_KEY= '<?= SHOPIFY_APP_API_KEY ?>';
            var SHOPIFY_APP_STORE = 'https://<?= $shopauth->shop ?>';
            var SHOPIFY_STORE = '<?= $shopauth->shop ?>';
        </script>
    </head>
    <body>

