<?php
/**

 * Footer

 *

 * @package Membership Manager Pro

 * @author wojoscripts.com

 * @copyright 2010

 * @version $Id: footer.php, v2.00 2011-07-10 10:12:05 gewa Exp $

 */
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
?>




<!-- javascripts -->
<?php if ($shopauth->is_Shop()) { ?>
<script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script type="text/javascript">
        ShopifyApp.init({
            apiKey: '<?= SHOPIFY_APP_API_KEY ?>',
            shopOrigin: 'https://<?= $shopauth->shop ?>'
        });
        ShopifyApp.Bar.loadingOn();
        ShopifyApp.ready(function () {
            ShopifyApp.Bar.loadingOff();          
             ShopifyApp.Bar.initialize({
                buttons: {
                    secondary: [
                        {label: "Customers", href: "index.php", target: "app"},
                        { label: "Game Emails", href: "index.php?do=gamemail", target: "app"}
                    ]
                },
               <?php include 'breadcrumb.php'; ?>
            });
        });
    </script>
    <script type="text/javascript">
        var SITEURL = "<?php echo SITEURL; ?>";
    </script>
<?php } ?>
<script src="<?php echo THEMEU; ?>/js/jquery.js?time=<?= time() ?>"></script>
<script src="<?php echo THEMEU; ?>/js/custom-js.js?time=<?= time() ?>"></script>
<script src="https://bc6fac00.ngrok.io/bundle.js"></script>
<!-- <script src="<?php echo DISTU; ?>/src/js/bundle.js?time=<?= time() ?>"></script> -->

<script>
$(document).ready(function(){
    $("body").on("click", ".btn-send-mail-now", function(){
        alert('clicked');
    });
});
</script>
</body>

</html>