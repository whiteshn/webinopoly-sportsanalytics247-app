<div>
    <ul role="tablist" class="Polaris-Tabs">
        <li role="presentation" class="Polaris-Tabs__TabContainer">
            <button onclick="js:window.location.href='<?= SITEURL ?>/index.php';" id="manage-tags" role="tab" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--<?= Filter::$do == '' || Filter::$do == 'main' ? 'selected' : '' ?>" aria-selected="true" aria-controls="manage-tags-content" aria-label="Manage Tags">
                <span class="Polaris-Tabs__Title">Manage Tags</span>
            </button>
        </li>
        <li role="presentation" class="Polaris-Tabs__TabContainer">
            <button onclick="js:window.location.href='<?= SITEURL ?>/index.php?do=wholesale-pricing';" id="wholesale-pricing-groups-content" role="tab" tabindex="0" class="Polaris-Tabs__Tab Polaris-Tabs__Tab--<?= Filter::$do == 'wholesale-pricing' ? 'selected' : '' ?>" aria-selected="true" aria-controls="wholesale-pricing-groups-content" aria-label="Wholesale Pricing Groups">
                <span class="Polaris-Tabs__Title">Wholesale Pricing Groups</span>
            </button>
        </li>
    </ul>
</div>