<?php
/**
 * Init
 *
 * @package Chili Portal
 * @author Nikunj Suthar <nik@webinopoly.com>
 * @copyright 2017
 * @version $Id: index.php, v4.00 2017-03-14 19:13:05 gewa Exp $
 */
//error_reporting(E_ALL);
ini_set('max_execution_time', 0);
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
?>
<?php


$BASEPATH = str_replace("init.php", "", realpath(__FILE__));
define("BASEPATH", $BASEPATH);

$configFile = BASEPATH . "lib/config.ini.php";
if (file_exists($configFile)) {
    require_once ($configFile);
} else {
    header("Location: setup/");
    exit;
}
require_once(BASEPATH . "lib/class_db.php");
require_once(BASEPATH . "lib/class_registry.php");
require_once(BASEPATH . "lib/class_curl.php");
require_once(BASEPATH . "lib/class_curl_response.php");
require_once('vendor/autoload.php');
require_once(BASEPATH . "lib/class_PriceRule.php");
require_once(BASEPATH . "lib/class_DraftOrder.php");
require_once(BASEPATH . "lib/class_graphql.php");



Registry::set('Database', new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE));
$db = Registry::get("Database");
$db->connect();


//Start Curl Class
Registry::set('Curl', new Curl());
$curl = Registry::get("Curl");

//Start DB Tools Class 
require_once(BASEPATH . "lib/class_dbtools.php");
Registry::set('Dbtools', new dbTools());
$Dbtools = Registry::get("Dbtools");

//Include Functions
require_once(BASEPATH . "lib/functions.php");

if (!defined("_PIPN")) {
    require_once(BASEPATH . "lib/class_filter.php");
    $request = new Filter();
}

//Start Core Class 
require_once(BASEPATH . "lib/class_core.php");
Registry::set('Core', new Core());
$core = Registry::get("Core");

//Start ArrayHelper Class
require_once(BASEPATH . "lib/ArrayHelper.php");
Registry::set('ArrayHelper', new ArrayHelper());
$arrayHelper = Registry::get("ArrayHelper");

//Start JsonHandler Class
require_once(BASEPATH . "lib/class_JsonHandler.php");
Registry::set('JsonHandler', new JsonHandler());
$JsonHandler = Registry::get("JsonHandler");

//Start Minify Class
require_once (BASEPATH . "lib/class_minify.php");
Registry::set('Minify', new Minify());

require_once(BASEPATH . "lib/class_shopauth.php");
Registry::set('Shopauth', new Shopauth());
$shopauth = Registry::get("Shopauth");

require_once(BASEPATH . "lib/class_products.php");
Registry::set('Products', new Products());
$Products = Registry::get("Products");

require_once(BASEPATH . "lib/class_Order.php");
Registry::set('Orders', new Order());
$Orders = Registry::get("Orders");

require_once(BASEPATH . "lib/class_customer.php");
Registry::set('Customers', new Customer());
$Customers = Registry::get("Customers");

require_once(BASEPATH . "lib/class_gameemail.php");
Registry::set('GameEmail', new GameEmail());
$GameEmail = Registry::get("GameEmail");



if (isset($_SERVER['HTTPS'])) {
    $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "https";
} else {
    $protocol = 'https';
}

//$dir = (Registry::get("Core")->site_dir) ? '/sportsanalytics247/' . Registry::get("Core")->site_dir : '';
$dir = (Registry::get("Core")->site_dir) ? '/' . Registry::get("Core")->site_dir : '';

$url = preg_replace("#/+#", "/", $_SERVER['HTTP_HOST'] . $dir);

$site_url = $protocol . "://" . $url;
define("SITEURL", $site_url);
define("THEME", BASEPATH . "/assets");
define("THEMEU", SITEURL . "/assets");

define("DIST", BASEPATH . "/dist");
define("DISTU", SITEURL . "/dist");

define("UPLOADS", BASEPATH . "uploads/");
define("UPLOADURL", SITEURL . "/uploads/");

define("UPLOAD_DOC", BASEPATH . "upload_document/");
define("UPLOAD_DOC_URL", SITEURL . "/upload_document/");
//?>