const {join} = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: [
        join(__dirname, 'dist/src/js/shopify-app.jsx'),
    ],
    output: {
        path: join(__dirname, 'dist/src/js'),
        filename: 'bundle.js',
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new HtmlWebpackPlugin({
            filename: 'index.php',
            template: './index.php',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                include: join(__dirname, 'dist/src/js'),
                use: [{
                        loader: 'babel-loader',
                        options: {
                            babelrc: false,
                            presets: ['env','react'],
                        },
                    }],
            },
            
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    devServer: {
        compress: true,
        disableHostCheck: true,
    }
};