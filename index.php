<?php
/**
 * Index
 *
 * @package Chili Portal
 * @author Nikunj Suthar <nik@webinopoly.com>
 * @copyright 2017
 * @version $Id: index.php, v4.00 2017-03-14 19:13:05 gewa Exp $
 */
define("_VALID_PHP", true);
require_once("./init.php");


if (isset($_GET['shop']) && isset($_GET['hmac']) && isset($_GET['timestamp']) && verifyHmac()) :
    $result = $shopauth->login($_GET['shop']);
    //Login successful
    if ($result):
        redirect_to("index.php");
    endif;
elseif ($shopauth->shopToken == ''):
    redirect_to("login.php");
endif;

?>

<?php include './include/header.php'; ?>

<div id="root">

</div>
<script type="text/javascript">
    var SITEURL = "<?php echo SITEURL; ?>";
    // get all customers
<?php $customersData = Customer::getCusotmers($shopauth->shop); ?>
    var customersData = <?= json_encode($customersData); ?>;
<?php if (isset($_GET['cust_id']) && isset($_GET['sub_type'])) { ?>
    <?php $subscriptionData = Order::getSubscriptions($shopauth->shop, $_GET['cust_id'], $_GET['sub_type']); ?>
        // get all subscriptions
        var subscriptionData = <?= json_encode($subscriptionData); ?>
<?php } ?>
<?php if (Filter::$do == 'gameemail-add') { ?>
    <?php $pkgOptions = Products::getPkgOptions($shopauth->shop); ?>
        // get all subscriptions
        var pkgOptions = <?= json_encode($pkgOptions); ?>
<?php } ?>
<?php if (Filter::$do == 'gamemail') { ?>
    <?php $gamemails = GameEmail::getGameEmailTemplates($shopauth->shop); ?>
        // get all subscriptions
        var gameMails = <?= json_encode($gamemails); ?>
<?php } ?>
<?php if (Filter::$do == 'send-mail-now') { ?>
    <?php 
        $specificSubscriptions = GameEmail::getSpecificSubscription($shopauth->shop, $_GET["sid"]); 

        #region - Request For Instant Mail
        if(count($specificSubscriptions) > 0){
            GameEmail::sendSubscriptionMailNow($shopauth->shop, $_GET["sid"]);
        }
        #endregion
    ?>
        var specificSubscriptions = <?= json_encode($specificSubscriptions); ?>
<?php } ?>

</script>
<?php include './include/footer.php'; ?>