<?php
/** * Login 
 * * * @package Membership Manager Pro 
 * * @author wojoscripts.com 
 * * @copyright 2015 
 * * @version $Id: login.php, v3.0 2015-02-05 10:12:05 gewa Exp $ */
define("_VALID_PHP", true);
require_once("./init.php");

use PHPShopify\ShopifySDK;
use PHPShopify\AuthHelper;
?>
<?php
if ($shopauth->is_Shop()) {
    redirect_to("index.php");
    exit;
}

try {
    if (!isset($_GET['code'])) {
        if ((isset($_POST['doLogin']) && $_POST['shop'] != '') || (isset($_GET['shop']) && isset($_GET['hmac']))) {
            if ($_POST['shop'] != '') {
                $shop = $_POST['shop'];
            } elseif ($_GET['shop'] != '') {
                $shop = $_GET['shop'];
            } else {
                extract($_REQUEST);
            }

            $config = array(
                'ShopUrl' => $shop,
                'ApiKey' => SHOPIFY_APP_API_KEY,
                'SharedSecret' => SHOPIFY_APP_SHARED_SECRET,
            );

            ShopifySDK::config($config);

            //your_authorize_url.php
            $scopes = 'write_product_listings,read_product_listings,unauthenticated_read_collection_listings,unauthenticated_read_product_listings,unauthenticated_write_checkouts,unauthenticated_write_customers,unauthenticated_read_content,read_draft_orders, write_draft_orders,read_checkouts, write_checkouts,read_content,write_content,read_themes,write_themes,read_products,write_products,read_customers,write_customers,read_orders,write_orders,read_script_tags,write_script_tags,read_fulfillments,write_fulfillments,read_shipping,write_shipping,read_price_rules, write_price_rules';

            //This is also valid
            //$scopes = array('read_products','write_products','read_script_tags', 'write_script_tags'); 
            $redirectUrl = SHOPIFY_APP_REDIRECT_URL;

            PHPShopify\AuthHelper::createAuthRequest($scopes, $redirectUrl);
        }
    } else {
        extract($_GET);

        //shop exist then login into shop
        $ExistshopRow = Registry::get("Core")->getRowByColumn(Shopauth::authTable, "shop_url='{$shop}'", false, false);
        if ($ExistshopRow->shop_url != '') {
            $result = Registry::get("Shopauth")->login($ExistshopRow->shop_url);
            if ($result) {
                redirect_to('index.php');
                return;
            }
        }

        $config = array(
            'ShopUrl' => $shop,
            'ApiKey' => SHOPIFY_APP_API_KEY,
            'SharedSecret' => SHOPIFY_APP_SHARED_SECRET,
        );

        ShopifySDK::config($config);
        $accessToken = PHPShopify\AuthHelper::getAccessToken();
        if ($accessToken != '') {
            $data = array(
                'shop_url' => $shop,
                'access_token' => $accessToken,
                'created_at' => @date('Y-m-d H:i:s')
            );
            $shopRow = Registry::get("Core")->getRowByColumn(Shopauth::authTable, "shop_url='{$shop}'", false, false);

            if (empty($shopRow)) {
                $lastid = $db->insert(Shopauth::authTable, $data);
            } else {
                $db->update(Shopauth::authTable, $data, "id={$shopRow->id}");
                $lastid = $shopRow->id;
            }

            if ($lastid > 0) {
                $shopCred = Registry::get("Core")->getRowById(Shopauth::authTable, $lastid);
                $config = array(
                    'ShopUrl' => $shopCred->shop_url,
                    'AccessToken' => $shopCred->access_token,
                );
                $shopify = new PHPShopify\ShopifySDK($config);
                $shopArr = $shopify->Shop->get();
                $updateData = array(
                    'shop_id' => $shopArr['id'],
                    'customer_email' => $shopArr['customer_email'],
                    'account_email' => $shopArr['email'],
                    'shop_owner' => $shopArr['shop_owner'],
                    'shop_name' => $shopArr['name']
                );
                $db->update(Shopauth::authTable, $updateData, "id={$shopCred->id}");

                // add uninstallWebhook
                Shopauth::uninstallWebhook($shopify, $shopCred->shop_url);
                Shopauth::productCreateWebhook($shopify, $shopCred->shop_url);
                Shopauth::productUpdateWebhook($shopify, $shopCred->shop_url);
                Shopauth::productDeleteWebhook($shopify, $shopCred->shop_url);
                Shopauth::orderCreateWebhook($shopify, $shopCred->shop_url);


                //login into shop
                $result = Registry::get("Shopauth")->login($shopCred->shop_url);
                if ($result) {
                    redirect_to('index.php');
                }
            }
        }
    }
} catch (Exception $e) {
    redirect_to('login.php');
} catch (PHPShopify\Exception $e) {
    redirect_to('login.php');
}
?><!DOCTYPE html><html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--
        <meta name="description" content="Karmanta - Bootstrap 3 Responsive Admin Template">
        <meta name="author" content="GeeksLabs">
        <meta name="keyword" content="Karmanta, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
        <link rel="shortcut icon" href="img/favicon.png">-->
        <title>Login | Sportsanalytics247</title>

        <link rel="stylesheet" href="https://sdks.shopifycdn.com/polaris/1.12.4/polaris.min.css" />
        <meta http-equiv="Content-Security-Policy" content="script-src 'self'">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var SITEURL = "<?php echo SITEURL; ?>";
        </script>
        <style type="text/css">
            @media (min-width: 640px){
                .dialog-heading {
                    font-size: 2.8rem;
                    line-height: 3.2rem;
                }
                .dialog-subheading {
                    font-size: 2rem;
                    line-height: 2.8rem;
                }
            }
            .dialog-heading {
                margin-top: 90px;
                margin-bottom: 0.8rem;
                font-size: 2.4rem;
                font-weight: 500;
                line-height: 2.8rem;
            }
            .dialog-heading, .dialog-subheading {
                text-align: center;
            }
            .dialog-subheading {
                margin-bottom: 4rem;
                color: #637381;
                font-size: 1.6rem;
                font-weight: 400;
                line-height: 2.4rem;
            }
            .login-container {
                max-width: 340px;
                margin: 2rem auto;
                margin-top: 1.6rem;
            }
        </style>
    </head>
    <body class="login-img2-body">
        <div class="Polaris-Page Polaris-Page--singleColumn">
            <div class="Polaris-Page__Header">
                <h1 class="dialog-heading">Sports Analytics 24/7</h1>
                <h2 class="dialog-subheading">Log in to manage your game e-mails and settings</h2>
            </div>
            <div class="Polaris-Page__Content">
                <form class="login-form" method="post">
                    <div class="login-container">
                        <div class="Polaris-FormLayout">
                            <div class="Polaris-FormLayout__Item">
                                <div class="">
                                    <div class="Polaris-TextField">
                                        <div class="Polaris-TextField__Prefix"><img style="margin-top:10px;" alt="" src="<?= THEMEU ?>/img/shopify.png" width="30"></div>
                                        <input name="shop" class="Polaris-TextField__Input" aria-labelledby="Shop Url" aria-invalid="false" value="" placeholder="demo.myshopify.com">
                                        <div class="Polaris-TextField__Backdrop"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-PageActions">
                            <input type="hidden" name="doLogin" value="1"/>
                            <button type="submit" class="Polaris-Button Polaris-Button--primary Polaris-Button--sizeLarge Polaris-Button--fullWidth">
                                <span class="Polaris-Button__Content">
                                    <span>Login or Install</span>
                                </span>
                            </button>
                        </div>
                    </div
                </form>
            </div>
        </div>
    </body>
</html>